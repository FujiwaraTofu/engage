import Engage
import Slab

en = Engage
sl = Slab

using Slab.Types

using LinearAlgebra: Diagonal

dxs = [1., 2., 4., 8.]
slm = en.SlabMesh(dxs)
I  = en.get_I(slm)
H  = en.get_H(slm)
Hf = en.get_H_f(slm)

G = 1

b_Σts = [1.0, 2.0, 4.0, 2.0]
b_Σss = [0.4, 0.9, 0.2, 0.3]
b_Σfs = [0.1, 0.2, 0.1, 0.4]
b_νs  = [2.3, 2.2, 2.4, 2.5]
b_Qs  = [0.2, 0.1, 0.9, 0.8]
b_χs  = ones(H)

# NOTE : Engage version construction

Σts = reshape(b_Σts, G, H)
Σss = reshape(b_Σss, G, G, H)
Σfs = reshape(b_Σfs, G, H)
νs  = reshape(b_νs , G, H)
χs  = reshape(b_χs , G, H)
Qs  = reshape(b_Qs , G, H)

dxfis = [0.5 * (dxs[i] + dxs[i+1]) for i in 1:(I-1)]
dxfs = [0. ; dxfis ; 0.]

Dps = Dms = en.gen_ds(slm, Σts)

Dhs = zeros(G, Hf)

mm = en.MSEDMaterial(Dps, Dms, Dhs, Σts, Σss, Σfs, νs, χs, Qs, G, H, Hf)

mL = en.gen_mL(mm, slm)
mS = en.gen_mS(mm, slm)
mF = en.gen_mF(mm, slm)
vQ = en.gen_vQ(mm, slm)
mM = mL - mS

# NOTE : Slab version construction

sl_me = sl.Mesh(sl.Cell.(dxs))
sl_ms = sl.Material.(b_Σts, b_Σss, b_Σfs, b_νs, b_Qs)
sl_lb, sl_rb = sl.Symmetry(), sl.Symmetry()

sl_L = sl.const_diff_lhs(sl_me, sl_ms, (sl_lb, sl_rb))
sl_Q = sl.const_diff_rhs_fs(sl_me, sl_ms, (sl_lb, sl_rb))
sl_F = sl.const_diff_rhs_eig(sl_me, sl_ms, (sl_lb, sl_rb))

t_ϕ = [3., 4., 8., 10.]

@show maximum(abs.((mM \ vQ) - (sl_L \ sl_Q)))
@show maximum(abs.(((mM) \ (mF*t_ϕ + vQ)) - ((sl_L) \ (Diagonal(sl_F)*t_ϕ + sl_Q))))
