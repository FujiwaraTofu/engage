import Engage
import Slab

en = Engage
sl = Slab

J = 500;

M = rand(J, J); F = rand(J,J)
sl_me = sl.Mesh(sl.Cell.(rand(J)))

ϕs_i = rand(J)
λ_i = rand()
λs_i = rand()

en_λsf = en.gen_λsf_parcs(0.99)
sl_λsf = sl.lambda_s_parcs

mi = 10;
cf = (any...) -> false # never converge

en_ϕss, en_λss = en.power_iterate(M, F;
                               ϕs_i = ϕs_i,
                               λ_i  = λ_i,
                               cf   = cf,
                               mi   = mi,
                               λsf  = en_λsf,
                               λs_i = λs_i,
                               save = true)
prepend!(en_ϕss, [ϕs_i])
prepend!(en_λss, λ_i)

sl_ϕss, sl_λss = sl.power_iterate(sl_me, M, F;
                                  scalar_fluxes_init = ϕs_i,
                                  lambda_init        = λ_i,
                                  conv_fun           = cf,
                                  max_iters          = mi,
                                  lambda_s_fun       = sl_λsf,
                                  lambda_s_init      = λs_i,
                                  save               = true)

@show maximum(abs.(en_ϕss[end] - sl_ϕss[end]))
@show abs(en_λss[end] - sl_λss[end])
