import Engage

en = Engage

dxs = [0.1, 0.3, 0.2]
dys = [0.4, 0.1]

I = length(dxs)
J = length(dys)

rm = en.ReMe(dxs, dys)
H   = en.get_H(rm)
H_f = en.get_H_f(rm)

G = 2
Σ_ts = [0.1 0.2 0.4 0.9 0.8 0.9;
        0.2 0.3 0.5 0.7 0.4 0.9]
Σ_ss = zeros(G, G, H)
Σ_fs = zeros(G, H)
νs   = zeros(G, H)
χs   = zeros(G, H)
Qs   = [0.7 0.1 0.8 0.7 0.9 0.1;
        0.9 0.1 0.9 0.4 0.4 0.2]

ntm = en.NeTrMa(Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H)

N_lqn = 2
μs_pr, ηs_pr, ξs_pr, ws_pr = en.gen_sqset_lqn(N_lqn)
N = 8 * length(ws_pr)

sfq = en.SyFuQu(N, μs_pr, ηs_pr, ξs_pr, ws_pr)

rfd = en.ReFiDi(en.αf_su, en.αf_su)

tp = en.TrPr(sfq, ntm, rm, rfd, N, G, H)

vtb  = en.VaTrBo()
itb1 = en.IsTrBo(0.25)
itb2 = en.IsTrBo(0.35)
itb3 = en.IsTrBo(0.90)

vtb_hfs  = en.get_h_f_rbs(rm)
itb1_hfs = en.get_h_f_lbs(rm)
itb2_hfs = en.get_h_f_ubs(rm)
itb3_hfs = en.get_h_f_dbs(rm)

h_fs_tbs = [vtb_hfs, itb1_hfs, itb2_hfs, itb3_hfs]
tbs      = [vtb    , itb1    , itb2    , itb3]
tbc = en.TrBoCo(h_fs_tbs, tbs)

ψds = en.gen_init_ψds(sfq, ntm, rm, rfd)

en.setup!(tp, tbc, ψds)

s_q = en.gen_s_q(sfq, ntm, rm)
en.march!(tp, s_q, ψds)

# cell-center angular fluxes
ψhs = en.gen_ψhs(sfq, ntm, rm, rfd, en.SC, 1:G, 1:H, ψds)
# cell-center scalar  fluxes
ϕs  = en.gen_ϕs(sfq, ψhs)

mm     = en.gen_MSEDMaterial(sfq, ntm, rm, rfd, ψds)

dbc     = en.gen_DiBoCo(sfq, ntm, rm, rfd, ψds, tbc)

mL  = en.gen_mL(mm, rm)
vQ  = en.gen_vQ(mm, rm)
mLb = en.gen_mLb(mm, rm, dbc)
vQb = en.gen_vQb(mm, rm, dbc)

dϕs = (mL + mLb) \ (vQ + vQb)

rs = (mL + mLb)*reshape(ϕs, G*H) - (vQ + vQb)

cis = CartesianIndices((G, I, J))
lis = LinearIndices((G, I, J))

ltϕs = reshape(ϕs, G*H)
ldϕs = dϕs

ctϕs = reshape(ϕs, G, I, J)
cdϕs = reshape(dϕs, G, I, J)

@show maximum(abs.(ltϕs - ldϕs))
