import Engage

en = Engage

import LinearAlgebra

la = LinearAlgebra

# using Profile
# using ProfileView

Fl = Float64

I = 40
J = 40
G = 4

gs = 1:G
is = 1:I
js = 1:J

rms  = collect(0.05:0.05:0.5)
rfs  = collect(0.10:0.10:0.5)
rois = collect(-I:1:I)
rojs = collect(-J:1:J)

gen_rp(m) = begin
    rm = rand(rms)
    rfi = rand(rfs)
    rfj = rand(rfs)
    roi = rand(rois)
    roj = rand(rojs)
    return [(1. + (i/I)*(j/J)*rm*cos(rfi*2*π*(i-roi)/I)*sin(rfj*2*π*(j-roj)/J))*m
            for i in is, j in js]
end

bdx = 0.50
bdy = 0.50
dxs = @. bdx + 0.05*sin(2*π*is/I)
dys = @. bdy + 0.05*sin(4*π*js/J)

rm = en.ReMe(dxs, dys)

H   = en.get_H(rm)
H_f = en.get_H_f(rm)

x_es = [0.; cumsum(dxs)]
y_es  = [0.; cumsum(dys)]
x_cs = 0.5 * (x_es[1:(end-1)] + x_es[2:end])
y_cs = 0.5 * (y_es[1:(end-1)] + y_es[2:end])
X = sum(dxs)
Y = sum(dys)

Σ_ts = cat([reshape(gen_rp(1.00), 1, H) for g in gs]..., dims=1)
Σ_fs = cat([reshape(gen_rp(0.02), 1, H) for g in gs]..., dims=1)
νs   = cat([reshape(gen_rp(2.4), 1, H) for g in gs]..., dims=1)
χs   = cat([reshape(gen_rp(2.4), 1, H) for g in gs]..., dims=1)
# NOTE : make sure sums to unity
for h in 1:H
    χs[:, h] = χs[:,h] ./ sum(χs[:,h])
end
Qs   = zeros(G, H)

tΣ_ss = 0.95 * (Σ_ts - Σ_fs)
Σ_ss = zeros(G, G, H)
  for h in 1:H
    for g in 1:G
        Σ_ss[g, :, h] .= tΣ_ss[g, h] / G
    end
end

ntm = en.NeTrMa(Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H)

rfd = en.ReFiDi(en.αf_su, en.αf_dd)

N_lqn = 8
μs_pr, ηs_pr, ξs_pr, ws_pr = en.gen_sqset_lqn(N_lqn)
N = 8 * length(ws_pr)
sfq = en.SyFuQu(N, μs_pr, ηs_pr, ξs_pr, ws_pr)

tp = en.TrPr(sfq, ntm, rm, rfd, N, G, H)

er(o, n; l=Inf) = begin
    return la.norm((@. (n - o) / (abs(n))), l)
end

tb = en.VaTrBo()
ntbc = 1
# tb = en.ReTrBo()
# ntbc = 4

tb_hfs  = en.get_h_f_bs(rm)

h_fs_tbs = [tb_hfs]
tbs      = [tb    ]

tbc = en.TrBoCo(h_fs_tbs, tbs)

ϵ   = 1.e-10
mi  = 2000
nis = 25

si_ϕs_o = ones(G,H)
si_λ_o  = 1.
si_ψds  = en.gen_init_ψds(sfq, ntm, rm, rfd)

si_eϕs = []
si_eλs = []
si_i = 0
si_t = @timed for i in 1:mi
    global si_ϕs_o
    global si_λ_o
    global si_ψds
    global si_i

    si_i = i
    if mod(si_i, nis) == 0
        println("si : i = $(si_i)")
    end

    ss = en.gen_ss(sfq, ntm, rm, si_ϕs_o; λ = si_λ_o)
    # may need to converge boundaries :)
    for _ in 1:ntbc
        en.march!(tp, ss, si_ψds)
        en.setup!(tp, tbc, si_ψds)
    end

    tψhs = en.gen_ψhs(sfq, ntm, rm, rfd, en.SC, 1:G, 1:H, si_ψds)
    tϕs  = en.gen_ϕs(sfq, tψhs)
    # NOTE : re-normalize
    tϕs = tϕs ./ la.norm(tϕs, 2)

    tλ = en.est_tran_λ(sfq, ntm, rm, rfd, si_ψds)

    if i != 1
        eϕ = er(si_ϕs_o, tϕs)
        eλ = er(si_λ_o, tλ)

        push!(si_eϕs, eϕ)
        push!(si_eλs, eλ)

        c = (eϕ < ϵ) && (eλ < ϵ)
    else
        c = false
    end

    si_ϕs_o = tϕs
    si_λ_o  = tλ

    if c
        break
    end
end

si_eϕs_ρs = en.gen_it_ρs(si_eϕs)
si_eϕs_ρ  = en.gen_es_ρ(si_eϕs)

si_eλs_ρs = en.gen_it_ρs(si_eλs)
si_eλs_ρ  = en.gen_es_ρ(si_eλs)

mean = (c) -> sum(c) / length(c)

f1 = (ω) -> @. atan(ω) / ω

i_ms = reshape(mapslices(mean, sum(reshape(Σ_ts, G, I, J) .* reshape(dxs, 1, I, 1), dims=2), dims=3), G)
j_ms = reshape(mapslices(mean, sum(reshape(Σ_ts, G, I, J) .* reshape(dys, 1, 1, J), dims=3), dims=2), G)

si_ρis = f1(π ./ i_ms)
si_ρjs = f1(π ./ j_ms)

[si_ρis si_ρjs]

nda_ϕs_o = ones(G,H)
nda_λ_o = 1.
nda_ψds = en.gen_init_ψds(sfq, ntm, rm, rfd)

g_picf(δ) = begin
    picf = (M, F, ϕs_p, ϕs_n, λ_p, λ_n) -> begin
        return ((er(ϕs_p, ϕs_n) < δ) && (er(λ_p, λ_n) < δ))
    end
end

ϵ_pi_o = 1.e-2 * ϵ
ϵ_pi_i = 1.e-1 * ϵ_pi_o
pi_cf_o = g_picf(ϵ_pi_o)
pi_cf_i = g_picf(ϵ_pi_i)

gz = (l) -> 0.
parcs_r = 0.9
sf = en.gen_λsf_parcs(parcs_r)

nda_eϕs = []
nda_eλs = []
nda_i = 0
nda_t = @timed for i in 1:mi
    global nda_ϕs_o
    global nda_λ_o
    global nda_ψds
    global nda_i

    nda_i = i
    if mod(nda_i, nis) == 0
        println("nda : i = $(nda_i)")
    end

    ss = en.gen_ss(sfq, ntm, rm, nda_ϕs_o; λ = nda_λ_o)
    for _ in 1:ntbc
        en.march!(tp, ss, nda_ψds)
        en.setup!(tp, tbc, nda_ψds)
    end

    # construct TCD materials
    mm = en.gen_MSEDMaterial(sfq, ntm, rm, rfd, nda_ψds)
    dbc = en.gen_DiBoCo(sfq, ntm, rm, rfd, nda_ψds, tbc)

    # println("power iteration: ")

    # mL  = en.gen_mL(mm, rm)
    # mS  = en.gen_mS(mm, rm)
    # mF  = en.gen_mF(mm, rm)
    # mLb = en.gen_mLb(mm, rm, dbc)

    # mM = (mL + mLb - mS)

    # @time dϕs, dλ, np = en.power_iterate(mM, mF;
    #                                ϕs_i = reshape(nda_ϕs_o, G*H),
    #                                λ_i  = nda_λ_o,
    #                                cf   = pi_cf_o)

    # println("msed")

    co_cgs = [[1:G]]
    vd      = length(co_cgs) # v-cycle depth :o
    co_n_pr = ones(vd)
    co_n_po = zeros(vd)
    co_λsf  = fill(sf, vd+1)
    co_λsif = fill(gz, vd+1)
    dϕs, dλ, np = en.msed_power_iterate(mm, rm, dbc,
                                        co_n_pr, co_n_po, co_cgs, co_λsf, co_λsif;
                                        ϕs_i = reshape(nda_ϕs_o, G*H),
                                        λ_i  = nda_λ_o,
                                        cfo  = pi_cf_o,
                                        cfi  = pi_cf_i)

    # reshape
    dϕs = reshape(dϕs, G, H)
    # normalize
    dϕs = dϕs ./ la.norm(dϕs, 2)

    if i != 1
        eϕ = er(nda_ϕs_o, dϕs)
        eλ = er(nda_λ_o, dλ)

        push!(nda_eϕs, eϕ)
        push!(nda_eλs, eλ)

        c = (eϕ < ϵ) && (eλ < ϵ)
    else
        c = false
    end

    nda_ϕs_o = dϕs
    nda_λ_o = dλ

    if c
        break
    end
end

nda_eϕs_ρs = en.gen_it_ρs(nda_eϕs)
nda_eϕs_ρ  = en.gen_es_ρ(nda_eϕs)

nda_eλs_ρs = en.gen_it_ρs(nda_eλs)
nda_eλs_ρ  = en.gen_es_ρ(nda_eλs)

@show si_t.time, nda_t.time;
@show abs((si_λ_o - nda_λ_o) / nda_λ_o)
mdϕ = maximum(@. abs(si_ϕs_o - nda_ϕs_o) / nda_ϕs_o)
@show mdϕ
