import Engage

en = Engage

using LinearAlgebra: norm

Fl = Float64

X = 1
Y = 2
G = 1 # NOTE : FIXED (only did MMS on mono-energ.)

sϕf = (x::Fl, y::Fl) -> cos(2*π*x/X)*cos(4*π*y/Y)
sσf = (x::Fl, y::Fl) -> cos(sin(π*(x/X-y/Y)))
sqf = (x::Fl, y::Fl) -> begin
    return (1/(3*X^2*Y^2))sec(sin(π*(x/X-y/Y)))*(2*π^2*Y^2*cos(π*(x/X-y/Y))*cos((4*π*y)/Y)*sin((2*π*x)/X)*tan(sin(π*(x/X-y/Y)))+cos((2*π*x)/X)*(cos((4*π*y)/Y)*(4*π^2*(4*X^2+Y^2)+3*X^2*Y^2*cos(sin(π*(x/X-y/Y)))^2)-4*π^2*X^2*cos(π*(x/X-y/Y))*sin((4*π*y)/Y)*tan(sin(π*(x/X-y/Y)))))
end

bI = 4
bJ = 8

scs = 2 .^ (collect(0:1:5))

ess = []
dss = []

for sc in scs
    @show sc

    I = sc * bI
    J = sc * bJ

    @show I, J

    dx = X / I
    dy = Y / J

    dxs = fill(dx, I)
    dys = fill(dy, J)

    rm  = en.ReMe(dxs, dys)
    H   = en.get_H(rm)
    H_f = en.get_H_f(rm)

    x_es = [0; cumsum(dxs)]
    y_es = [0; cumsum(dys)]

    x_cs = 0.5*(x_es[1:(end-1)] + x_es[2:end])
    y_cs = 0.5*(y_es[1:(end-1)] + y_es[2:end])

    sϕ   = [sϕf(x,y) for x in x_cs, y in y_cs]
    Σ_ts = reshape([sσf(x,y) for x in x_cs, y in y_cs], G, H)
    Qs   = reshape([sqf(x,y) for x in x_cs, y in y_cs], G, H)

    Σ_ss = zeros(G, G, H)
    Σ_fs = zeros(G, H)
    νs   = zeros(G, H)
    χs   = zeros(G, H)

    D_hs = zeros(G, H_f)

    Ds = en.gen_ds(rm, Σ_ts)

    D_ps = D_ms = Ds

    mm = en.MMa(D_ps, D_ms, D_hs, Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H, H_f)

    mL = en.gen_mL(mm, rm)
    vQ = en.gen_vQ(mm, rm)

    dϕ = reshape(mL \ vQ, I, J)

    ds = dϕ - sϕ

    push!(dss, ds)

    er = (1/length(ds))*norm(ds, 2)
    push!(ess, er)
end

ers = [ess[n-1] / ess[n] for n in 2:length(ess)]
@show ers
