import Engage
import Slab

using SparseArrays: spzeros

en = Engage
sl = Slab

N = 4
G = 2
H = 3

μs_pr, ws_pr = en.gen_qset_gl(N)

spq = en.SyPoQu(N, μs_pr, ws_pr)
μs = en.get_μs(spq)
ws = en.get_ws(spq)

dxs = [0.1, 0.3, 0.5]
sm = en.SlMe(dxs)
H_f = en.get_H_f(sm)

Σ_ts = reshape([1.0 0.9 0.7;
                5.0 4.0 2.0], G, H)
Σ_ss = zeros(G,G,H)
Σ_fs = zeros(G,H)
νs   = zeros(G,H)
χs   = zeros(G,H)
Qs   = zeros(G,H)

ntm = en.NeTrMa(Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H)

Ss = reshape([0.9 1.2 0.8;
              1.0 3.0 9.0], G, H)

sfd = en.SlFiDi(en.αf_dd)

tp = en.TrPr(spq, ntm, sm, sfd, N, G, H)

ψds = en.gen_init_ψds(spq, ntm, sm, sfd)

en.march!(tp, Ss, ψds)
ψhs = en.gen_ψhs(spq, ntm, sm, sfd, en.SC, 1:G, 1:H, ψds)

eϕes  = en.gen_ϕs(spq, ψds)

eϕhs  = en.gen_ϕs(spq, ψhs)

ejis = en.gen_jis(spq, ψds)
ejns = en.gen_jns(spq, sm, ψds)

eds = en.gen_ds(sm, Σ_ts)

edhs = en.gen_dhs(sm, eϕhs, ejns, eds)

ebs = en.gen_bs(spq, ntm, sm, sfd, ψds)

for g in 1:G
    sl_me = sl.Mesh(sl.Cell.(dxs))
    sl_ms = sl.Material.(Σ_ts[g,:], zeros(H), zeros(H), zeros(H), zeros(H))
    sl_ss = sl.Isotropic.((1/2)*Ss[g,:])
    sl_lb, sl_rb = sl.Incident(zeros(div(N,2))), sl.Incident(zeros(div(N,2)))
    sl_μs, sl_ws = sl.gauss_legendre(N)
    sl_svs = sl.FD.(fill(sl.alpha_dd, H))

    sfs = [sl.cell_scalar_flux_save_fun!,
           sl.face_current_save_fun!,
           sl.boundary_correction_save_fun!]
    sds = [sl.cell_save_data_init(sl_me),
           sl.face_save_data_init(sl_me),
           sl.s_face_save_data_init(sl_me)]
    sl.sweep(sl_me, sl_ms, sl_svs, sl_ss, (sl_lb, sl_rb), sl_μs, sl_ws;
             save_funs! = sfs,
             save_datas = sds)

    sϕ, sj, sbn = sds

    sl_dhs = sl.calc_d_hats(sl_me, sl_ms, sϕ, sj)

    sl_bs = spzeros(H_f)
    sl_bs[1] = sbn[1]/sϕ[1]
    sl_bs[H_f] = sbn[H_f]/sϕ[H]

    @show maximum(abs.(sϕ - eϕhs[g,:]))
    @show maximum(abs.(sj[2:end-1] - ejns[g,2:end-1]))
    @show maximum(abs.(sl_dhs - edhs[g, 2:end-1]))
    @show maximum(abs.(sl_bs - ebs[g,:]))
end
