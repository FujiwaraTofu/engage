import Engage
import Slab

en = Engage
sl = Slab

using Slab.Types

using Plots

H = 100

ϕss = []

for αt in [:dd, :su, :sc]

    X = 10.

    N = 8
    G = 5

    hs = 1:H

    n_dx =  X / H
    dxs = @. (1 + 0.1*sin(2*pi*hs/H))*n_dx
    es = [0; cumsum(dxs)]
    xs = 0.5 * (es[1:end-1] + es[2:end])

    Σtgfs = reshape(collect(1:G), 1, G)
    Qgfs  = reshape(collect(1:G), 1, G) .+ 2
    Σts = permutedims(@. (1 + 0.1*sin(Σtgfs*pi*xs/X)) * Σtgfs)
    Qs  = permutedims(@. (1 + 0.2*cos(Σtgfs*pi*xs/X)) * Qgfs)

    ns_gl, ws_gl = en.gen_qset_gl(N)

    hc_r = en.HeCo(en.HC_ID_R)
    hc_l = en.HeCo(en.HC_ID_L)

    spq = en.SyPoQu(N, ns_gl, ws_gl)

    Σss = zeros(G, G, H)
    Σfs = zeros(G, H)
    νs  = zeros(G, H)
    χs  = zeros(G, H)

    ntm = en.NeTrMa(Σts, Σss, Σfs, νs, χs, Qs, G, H)

    sm  = en.SlMe(dxs, H)

    if     αt == :dd
        en_αf = en.αf_dd
    elseif αt == :su
        en_αf = en.αf_su
    elseif αt == :sc
        en_αf = en.αf_sc
    else
        error()
    end

    sd = en.SlFiDi(en_αf)

    Ss  = en.gen_s_q(spq, ntm, sm)
    ψds = en.gen_init_ψds(spq, ntm, sm, sd)

    tp = en.TranProb(spq, ntm, sm, sd, N, G, H)

    @time en.march!(tp, Ss, ψds)

    ψhs = en.gen_ψhs(spq, ntm, sm, sd, en.SC, en.get_gs(ntm), en.get_hs(ntm), ψds)
    ws = en.get_ws(spq)
    ϕhs = en.gen_ϕs(spq, ψhs)

    if     αt == :dd
        global ϕs_dd = ϕhs
    elseif αt == :su
        global ϕs_su = ϕhs
    elseif αt == :sc
        global ϕs_sc = ϕhs
    else
        error()
    end

    # NOTE : Slab implementation
    alpha_st(c::sl.Cell, m::sl.Material, μs::VF) = begin
        sign.(μs)
    end
    alpha_sc(c::sl.Cell, m::sl.Material, μs::VF) = begin
        h  = sl.get_cell_width(c)
        Σt = sl.get_Σ_t(m)

        τ = Σt * h ./ μs
        return coth.(τ / 2) - 2. ./ τ
    end

    sl_me   = sl.Mesh(sl.Cell.(dxs))
    sl_mas  = [sl.Material.(Σts[g,:], zeros(H), zeros(H), zeros(H), Qs[g,:]) for g in 1:G]
    if     αt == :dd
        αf = sl.alpha_dd
    elseif αt == :su
        αf = alpha_st
    elseif αt == :sc
        αf = alpha_sc
    else
        error()
    end
    sl_slvs = fill(sl.FD(αf), H)
    sl_Ss   = [sl.fixed_source.(sl_mas[g][:], zeros(H)) for g in 1:G]
    sl_lb   = sl.Incident(zeros(div(N,2)))
    sl_rb   = sl.Incident(zeros(div(N,2)))
    sl_μs, sl_ws = sl.gauss_legendre(N)

    sl_ϕs = zeros(G, H)
    @time for g in 1:G
        sav_dats  = [sl.cell_save_data_init(sl_me)]
        sav_funs! = [sl.cell_scalar_flux_save_fun!]
        sl.sweep(sl_me, sl_mas[g], sl_slvs, sl_Ss[g],
                (sl_lb, sl_rb), sl_μs, sl_ws;
                save_funs! = sav_funs!,
                save_datas = sav_dats)
        ϕs, = sav_dats
        sl_ϕs[g, :] = reshape(ϕs, 1, H)
    end

    @show maximum(abs.(ϕhs - sl_ϕs))

    push!(ϕss, ϕhs)
end
