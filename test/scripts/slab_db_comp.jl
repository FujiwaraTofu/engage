import Engage
import Slab

en = Engage
sl = Slab

using Slab.Types

using LinearAlgebra: Diagonal

X = 10.

G = 1
H = 100
H_f = H + 1
I = H

hs = 1:H

ndx =  X / H
dxs = @. (1 + 0.1*sin(2*pi*hs/H))*ndx
es = [0; cumsum(dxs)]
xs = 0.5 * (es[1:end-1] + es[2:end])

nΣt = 1.0
nΣs = 0.9
nΣf = 0.4
nν  = 2.8
nQ  = 0.8

# NOTE : test scalar flux vector (for oper. comp)
t_ϕ = @. (3 + 0.4*sin(4*pi*xs/X)) * 1.

Σts = @. (1 + 0.1*sin(  pi*xs/X)) * nΣt
Σss = @. (1 + 0.2*cos(2*pi*xs/X)) * nΣs
Σfs = @. (1 + 0.2*sin(4*pi*xs/X)) * nΣf
νs = @. (1 - 0.1*cos(3*pi*xs/X)) * nν
χs = ones(H)
Qs = @. (1 - 0.2*cos(6*pi*xs/X)) * nQ

en_Σts = reshape(Σts, G, H)
en_Σss = reshape(Σss, G, G, H)
en_Σfs = reshape(Σfs, G, H)
en_νs  = reshape(νs , G, H)
en_χs  = reshape(χs , G, H)
en_Qs  = reshape(Qs , G, H)

# NOTE : Engage
en_me  = en.SlMe(dxs)
en_ntm = en.NeTrMa(en_Σts, en_Σss, en_Σfs, en_νs, en_χs, en_Qs, G, H)
en_Dps = en_Dms = en.gen_ds(en_me, en_Σts)
Dhs = @. 0.05*cos(0.5*pi*es/X)
Dhs[[1, end]] .= 0.
en_Dhs = reshape(Dhs, G, H_f)
sl_Dhs = Dhs[2:end-1]
en_mm = en.MSEDMaterial(en_Dps, en_Dms, en_Dhs, en_Σts, en_Σss, en_Σfs, en_νs,
                        en_χs, en_Qs, G, H, H_f)

# NOTE : Slab
sl_me = sl.Mesh(sl.Cell.(dxs))
sl_ntm = sl.Material.(Σts, Σss, Σfs, νs, Qs)

en_sy = en.ReflDiffBound()
sl_sy = sl.Symmetry()

Bl, Br  = 0.4, -5.4
jpl, jpr = -1.3, 4.5

sl_inl = sl.TCI(jpl, Bl)
sl_inr = sl.TCI(jpr, Br)

en_bs = zeros(G, H_f)
en_bs[1, [1, H_f]] = [Bl, Br]

en_js = zeros(G, H_f)
en_js[1, [1, H_f]] = [jpl, jpr]

en_in = en.InTCDiBo(en_bs, en_js)

sl_bs_ss = (sl_sy , sl_sy )
sl_bs_si = (sl_sy , sl_inr)
sl_bs_is = (sl_inl, sl_sy )
sl_bs_ii = (sl_inl, sl_inr)

# NOTE : check a couple different boundary condition combinations :D
en_co_ss = en.DiBoCo([[1, H_f]], [en_sy])
en_co_si = en.DiBoCo([[1], [H_f]], [en_sy, en_in])
en_co_is = en.DiBoCo([[1], [H_f]], [en_in, en_sy])
en_co_ii = en.DiBoCo([[1, H_f]], [en_in])

en_mL = en.gen_mL(en_mm, en_me)
en_mS = en.gen_mS(en_mm, en_me)
en_mF = en.gen_mF(en_mm, en_me)
en_vQ = en.gen_vQ(en_mm, en_me)

en_mLb_ss = en.gen_mLb(en_mm, en_me, en_co_ss)
en_mLb_si = en.gen_mLb(en_mm, en_me, en_co_si)
en_mLb_is = en.gen_mLb(en_mm, en_me, en_co_is)
en_mLb_ii = en.gen_mLb(en_mm, en_me, en_co_ii)

en_vQb_ss = en.gen_vQb(en_mm, en_me, en_co_ss)
en_vQb_si = en.gen_vQb(en_mm, en_me, en_co_si)
en_vQb_is = en.gen_vQb(en_mm, en_me, en_co_is)
en_vQb_ii = en.gen_vQb(en_mm, en_me, en_co_ii)

# NOTE : check matrices are equiv

# check F (fission matrix)
en_f = en.gen_mF(en_mm, en_me)
# NOTE : slab unaffected by b.c's (as long as they are valid i.e. no pos. inc.
# curr.)
sl_f = Diagonal(sl.const_diff_rhs_eig(sl_me, sl_ntm, (sl.Symmetry(), sl.Symmetry())))

# check (vQ + vQb) (inhomogeneous source + boundary inhomog. terms)
# NOTE : slab by default returns Q = (vQ + vQb)
sl_Q_ss = sl.const_diff_rhs_fs(sl_me, sl_ntm, sl_bs_ss)
sl_Q_si = sl.const_diff_rhs_fs(sl_me, sl_ntm, sl_bs_si)
sl_Q_is = sl.const_diff_rhs_fs(sl_me, sl_ntm, sl_bs_is)
sl_Q_ii = sl.const_diff_rhs_fs(sl_me, sl_ntm, sl_bs_ii)

sl_M_ss = sl.const_diff_lhs(sl_me, sl_ntm, sl_bs_ss, d_hats = sl_Dhs)
en_M_ss = (en_mL + en_mLb_ss - en_mS)

sl_M_si = sl.const_diff_lhs(sl_me, sl_ntm, sl_bs_si, d_hats = sl_Dhs)
en_M_si = (en_mL + en_mLb_si - en_mS)

sl_M_is = sl.const_diff_lhs(sl_me, sl_ntm, sl_bs_is, d_hats = sl_Dhs)
en_M_is = (en_mL + en_mLb_is - en_mS)

sl_M_ii = sl.const_diff_lhs(sl_me, sl_ntm, sl_bs_ii, d_hats = sl_Dhs)
en_M_ii = (en_mL + en_mLb_ii - en_mS)

@show maximum(abs.(  ((en_M_ss) \ (en_f*t_ϕ + en_vQ + en_vQb_ss))
                   - ((sl_M_ss) \ (sl_f*t_ϕ + sl_Q_ss))))
@show maximum(abs.(  ((en_M_si) \ (en_f*t_ϕ + en_vQ + en_vQb_si))
                   - ((sl_M_si) \ (sl_f*t_ϕ + sl_Q_si))))
@show maximum(abs.(  ((en_M_is) \ (en_f*t_ϕ + en_vQ + en_vQb_is))
                   - ((sl_M_is) \ (sl_f*t_ϕ + sl_Q_is))))
@show maximum(abs.(  ((en_M_ii) \ (en_f*t_ϕ + en_vQ + en_vQb_ii))
                   - ((sl_M_ii) \ (sl_f*t_ϕ + sl_Q_ii))))
