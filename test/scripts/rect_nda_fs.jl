import Engage
import Slab

en = Engage
sl = Slab

import LinearAlgebra

la = LinearAlgebra

Fl = Float64

# TODO : put in Engage
gen_it_ρs(es) = begin
    it_ρs = [es[i+1] / es[i] for i in 1:(length(es) - 1)]
    return it_ρs
end

MIN_US = 2

# df / db : percentage of iterated collection to drop at the
# start and end for global spectral radius estimation
gen_es_ρ(es; df::Fl=0.25, db::Fl=0.25) = begin
    n_es = length(es) # no. es
    n_df = Int(ceil(n_es * df)) # no. drop es (front)
    n_db = Int(ceil(n_es * db)) # no. drop es (back)

    # no. es used
    n_us = (n_es - n_df - n_db)

    if n_us < MIN_US
        error("insufficient number of iterations")
    end

    us =  es[(n_df+1):(end-n_db)]

    # slope, and intercept
    sl, inter = [1:n_us ones(n_us)] \ log.(us)

    return exp(sl)
end

I = 10
J = 12
G = 2

gs = 1:G
is = 1:I
js = 1:J

rms  = collect(0.005:0.005:0.05)
rfs  = collect(0.10:0.10:0.5)
rois = collect(-I:1:I)
rojs = collect(-J:1:J)

gen_rp(m) = begin
    rm = rand(rms)
    rfi = rand(rfs)
    rfj = rand(rfs)
    roi = rand(rois)
    roj = rand(rojs)
    return [(1. + (i/I)*(j/J)*rm*cos(rfi*2*π*(i-roi)/I)*sin(rfj*2*π*(j-roj)/J))*m
            for i in is, j in js]
end

bdx = 0.25
bdy = 0.25
dxs = @. bdx + 0.1*sin(2*π*is/I)
dys = @. bdy + 0.2*sin(4*π*js/J)

rm = en.ReMe(dxs, dys)

H   = en.get_H(rm)
H_f = en.get_H_f(rm)

x_es = [0.; cumsum(dxs)]
y_es  = [0.; cumsum(dys)]
x_cs = 0.5 * (x_es[1:(end-1)] + x_es[2:end])
y_cs = 0.5 * (y_es[1:(end-1)] + y_es[2:end])
X = sum(dxs)
Y = sum(dys)

Σ_ts = cat([reshape(gen_rp(1.00), 1, H) for g in gs]..., dims=1)
Qs   = cat([reshape(gen_rp(2.00), 1, H) for g in gs]..., dims=1)
Σ_fs = zeros(G,H)
νs   = zeros(G,H)
χs   = zeros(G,H)

tΣ_ss = 0.95 * (Σ_ts - Σ_fs)
Σ_ss = zeros(G, G, H)
  for h in 1:H
    for g in 1:G
        Σ_ss[g, :, h] .= tΣ_ss[g, h] / G
    end
end

ntm = en.NeTrMa(Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H)

rfd = en.ReFiDi(en.αf_su, en.αf_dd)

N_lqn = 4
μs_pr, ηs_pr, ξs_pr, ws_pr = en.gen_sqset_lqn(N_lqn)
N = 8 * length(ws_pr)
sfq = en.SyFuQu(N, μs_pr, ηs_pr, ξs_pr, ws_pr)

tp = en.TrPr(sfq, ntm, rm, rfd, N, G, H)

for (tb, tbn) in zip([en.VaTrBo(), en.IsTrBo(10.), en.ReTrBo()],
                     ["Vac."     , "Isot."       , "Refl."])
    @show tbn

    tb_hfs  = en.get_h_f_bs(rm)

    h_fs_tbs = [tb_hfs]
    tbs      = [tb    ]
    tbc = en.TrBoCo(h_fs_tbs, tbs)

    er(o, n; l=Inf) = begin
        return la.norm((@. (n - o) / (abs(n))), l)
    end

    ϵ   = 1.e-10
    mi  = 500
    nis = 25

    si_ϕs_o = zeros(G, H)
    si_ψds = en.gen_init_ψds(sfq, ntm, rm, rfd)

    si_es = []
    si_i = 0
    si_t = @timed for i in 1:mi
        si_ϕs_o
        si_ψds
        si_i

        si_i = i
        if mod(si_i, nis) == 0
            println("si : i = $(si_i)")
        end

        ss = en.gen_ss(sfq, ntm, rm, si_ϕs_o)
        en.march!(tp, ss, si_ψds)
        en.setup!(tp, tbc, si_ψds)

        tψhs = en.gen_ψhs(sfq, ntm, rm, rfd, en.SC, 1:G, 1:H, si_ψds)
        tϕs  = en.gen_ϕs(sfq, tψhs)

        if i != 1
            e = er(si_ϕs_o, tϕs; l=Inf)
            push!(si_es, e)

            c = e < ϵ
        else
            c = false
        end

        si_ϕs_o = tϕs

        if c
            break
        end
    end

    si_it_ρs = gen_it_ρs(si_es)
    si_es_ρ  = gen_es_ρ(si_es)

    nda_ϕs_o = zeros(G, H)
    nda_ψds = en.gen_init_ψds(sfq, ntm, rm, rfd)

    nda_es = []
    nda_i = 0
    nda_t = @timed for i in 1:mi
        nda_ϕs_o
        nda_ψds
        nda_i

        nda_i = i
        if mod(nda_i, nis) == 0
            println("si : i = $(nda_i)")
        end

        ss = en.gen_ss(sfq, ntm, rm, nda_ϕs_o)
        en.march!(tp, ss, nda_ψds)
        en.setup!(tp, tbc, nda_ψds)

        # construct TCD materials
        mm = en.gen_MSEDMaterial(sfq, ntm, rm, rfd, nda_ψds)
        dbc = en.gen_DiBoCo(sfq, ntm, rm, rfd, nda_ψds, tbc)

        mL  = en.gen_mL(mm, rm)
        mS  = en.gen_mS(mm, rm)
        vQ  = en.gen_vQ(mm, rm)
        mLb = en.gen_mLb(mm, rm, dbc)
        vQb = en.gen_vQb(mm, rm, dbc)

        dϕs = reshape((mL - mS + mLb) \ (vQ + vQb), G, H)

        if i != 1
            e = er(nda_ϕs_o, dϕs; l=Inf)
            push!(nda_es, e)

            c = e < ϵ
        else
            c = false
        end

        nda_ϕs_o = dϕs

        if c
            break
        end
    end

    nda_it_ρs = gen_it_ρs(nda_es)
    nda_es_ρ  = gen_es_ρ(nda_es)

    @show si_i, nda_i
    @show si_t.time, nda_t.time
    @show si_es_ρ, nda_es_ρ
    merr = maximum(@. abs((nda_ϕs_o - si_ϕs_o) / nda_ϕs_o))
    @show merr
end
