import Engage
import Mule

en = Engage
mu = Mule

using LinearAlgebra: eigvals

G  = 3
H  = 5
Hf = H + 1

dxs = rand(H)
Σts = rand(G,H)
Σss = rand(G,G,H)
Σfs = rand(G,H)
χs  = rand(G,H)
νs  = rand(G,H)
Qs  = rand(G,H)

Ds  = @. 1 / (3 * Σts)

# face coeff.'s for Engage
Dpis = [(1. / 3.) * (dxs[i] + dxs[i+1])/(Σts[g, i]*dxs[i] + Σts[g, i+1]*dxs[i+1])
        for g in 1:G, i in 1:(H-1)]
Dps = Dms = [zeros(G) Dpis zeros(G)]
Dhs = zeros(G, Hf)

lb, rb = mu.RDB(), mu.RDB()

dp = mu.DP(G, H, dxs, Ds, Σts, Σss, χs, νs.*Σfs, Qs, lb, rb)

mm  = en.MSEDMaterial(Dps, Dms, Dhs, Σts, Σss, Σfs, νs, χs, Qs, G, H, Hf)
slm = en.SlMe(dxs)

en_mL = en.gen_mL(mm, slm)
en_mS = en.gen_mS(mm, slm)
en_vQ = en.gen_vQ(mm, slm)
en_mF = en.gen_mF(mm, slm)

mu_mM = mu.const_diffus_M(dp)
mu_vQ = mu.const_diffus_Q(dp)
mu_mF = mu.const_diffus_F(dp)

en_fs_sol = (en_mL - en_mS) \ en_vQ
mu_fs_sol = mu_mM \ mu_vQ

@show maximum(abs.(en_fs_sol - mu_fs_sol))

en_λs = eigvals(Matrix((en_mL - en_mS)), Matrix(en_mF))
mu_λs = eigvals(Matrix(mu_mM), Matrix(mu_mF))

en_λs = filter((l) -> ! isinf(l) && abs(l) < 10^10, en_λs)
mu_λs = filter((l) -> ! isinf(l) && abs(l) < 10^10, mu_λs)

@show maximum(abs.(en_λs - mu_λs))
