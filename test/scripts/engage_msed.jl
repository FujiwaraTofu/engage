import Engage
import LinearAlgebra

en = Engage
la = LinearAlgebra

I   = 200
H   = I
H_f = I + 1
G   = 20

gs = 1:G
is = 1:I

rms = collect(0.005:0.005:0.05)
rfs = collect(0.5:0.5:5.0)
ros = collect(-I:1:I)

gen_rp(m) = begin
    rm = rand(rms)
    rf = rand(rfs)
    ro = rand(ros)
    return @. (1. + rm*cos(rf*2*π*(is - ro)/I))*m
end

dxs = gen_rp(1.)

sm = en.SlMe(dxs)

x_es = [0.; cumsum(dxs)]
x_cs = 0.5 * (x_es[1:(end-1)] + x_es[2:end])
X = sum(dxs)

Σ_ts = vcat([reshape(gen_rp(1.0), 1, I) for g in gs]...)
Σ_fs = vcat([reshape(gen_rp(0.01), 1, I) for g in gs]...)
νs   = vcat([reshape(gen_rp(2.3), 1, I) for g in gs]...)
χs   = rand(G); χs = repeat(χs / sum(χs), outer=(1,H))
Qs   = zeros(G, I)

D_ps  = D_ms = en.gen_ds(sm, Σ_ts)
nxem = repeat(reshape((1/X)*x_es,  1, H_f), outer = (G, 1))
rmm  = repeat(rand(rms,G), outer=(1,H_f))
rfm  = repeat(rand(rfs,G), outer=(1,H_f))
rom  = repeat(2. * rand(G).-1., outer=(1,H_f))
D_hs  = map((xe, m, f, o) -> m*cos(2*π*f*(xe-o)), nxem, rmm, rfm, rom)
D_hs[:,[1,H_f]] .= 0.

# NOTE : total scattering macro xs
tΣ_ss = Σ_ts - Σ_fs
Σ_ss = zeros(G, G, I)
for i in 1:I
    for g in 1:G
         Σ_ss[g, :, i] .= tΣ_ss[g, i] / G
    end
end

ce(co; n=Inf) = [la.norm(co[i] - co[i-1], n) for i in 2:length(co)]

mm  = en.MMa(D_ps, D_ms, D_hs, Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H, H_f)
h_fs_dbs = [en.get_h_f_bs(sm)]
dbs      = [en.ReflDiffBound()]
dbc = en.DiBoCo(h_fs_dbs, dbs)

# goal residual
goϵ = 1.e-9  # outer
giϵ = 1.e-10 # inner-most

cfo = en.gen_res_cf_pi(goϵ)
cfi = en.gen_res_cf_pi(giϵ)

mo = Inf
mi = Inf

# ref
mL  = en.gen_mL(mm, sm)
mLb = en.gen_mLb(mm, sm, dbc)
mS  = en.gen_mS(mm, sm)
mF  = en.gen_mF(mm, sm)
mM  = mL + mLb - mS

# ref. params
rϕs_i = ones(G*H)
rλ_i  = 1.
parcs_r = 0.90
rλsf = en.gen_λsf_parcs(parcs_r)
rλs_i = 0.
rsave = true

cr(ϕ, λ) = begin
    return la.norm(mM*ϕ - λ*mF*ϕ, Inf)
end

ro = @timed en.power_iterate(mM, mF;
                             ϕs_i = rϕs_i, λ_i  = rλ_i,
                             cf   = cfo  , mi   = mo,
                             λsf  = rλsf , λs_i = rλs_i,
                             save = rsave)
rϕss, rλss, rni = ro.value
rt = ro.time # seconds

rϕs = rϕss[end];
rλ  = rλss[end];
rts = (rt / rni) * collect(1:rni)

rrs = [cr(ϕ, λ) for (ϕ, λ) in zip(rϕss, rλss)]

gz = (l) -> rλs_i

# testing MSED V-Cycle :o :D ;|
co_cgs = [[1:20]]
vd      = length(co_cgs) # v-cycle depth :o
co_n_pr = ones(vd)
co_n_po = ones(vd)

λsf  = rλsf
λsif = gz

co_λsf  = fill(λsf , vd+1)
co_λsif = fill(λsif, vd+1)

vo = @timed en.msed_power_iterate(mm, sm, dbc, co_n_pr, co_n_po, co_cgs, co_λsf, co_λsif;
                                  ϕs_i = rϕs_i, λ_i = rλ_i,
                                  cfo = cfo, mo = mo,
                                  cfi = cfi, mi = mi,
                                  save = true)
vϕss, vλss, vni = vo.value
vt = vo.time # seconds

vϕs = vϕss[end];
vλ  = vλss[end];
vts = (vt / vni) * collect(1:vni)

vrs = [cr(ϕ, λ) for (ϕ, λ) in zip(vϕss, vλss)]

nor_fun = (ϕ) -> ϕ / la.norm(ϕ, 2)
no_rϕs = nor_fun(rϕs)
no_vϕs = nor_fun(vϕs)

@show abs(rλ - vλ)
@show maximum(abs.(no_rϕs - no_vϕs))

@show rt, vt

# NOTE : old
# col = collect
# co_cgs = [[1:10, 11:20], [1:2]]
# co_cgs = [[1:5, 6:10, 11:15, 16:20], [1:2, 3:4], [1:2]]
# co_cgs = [[col(1:4:G), col(2:4:G), col(3:4:G), col(4:4:G)], [[1,3], [2,4]], [1:2]]

