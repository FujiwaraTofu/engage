# 1 TODO : make is_valid functions for h_f and h
# 2 TODO : comprehensive error checks
module Engage

import FastGaussQuadrature
import LinearAlgebra
import SparseArrays
using Strided

# module aliases
fg = FastGaussQuadrature
la = LinearAlgebra
sa = SparseArrays

# type aliases
Fl    = Float64

Arr   = Array
AbArr = AbstractArray

Vec = Vector
Mat = Matrix

AbVec = AbstractVector
AbMat = AbstractMatrix

Fun = Function

# NOTE : utility functions
ispositive(n) = n > 0
isnegative(n) = n < 0

mean(s) = sum(s) / length(s)

# NOTE : line quadrature (only return principal values (ns > 0))

# Gauss Legendre (symmetric)
gen_qset_gl(N::Int) = begin
    ns, ws = fg.gausslegendre(N)
    inds_pr = div(N,2) .+ (1:div(N,2))
    ns_pr, ws_pr = ns[inds_pr], ws[inds_pr]
    return ns_pr, ws_pr
end

# uniform (symmetric)
gen_qset_uni(N::Int) = begin
    w  = 2. / N
    ns_pr = (w/2.) .+ w*collect(0:(div(N,2)-1))
    ws_pr = fill(w, div(N, 2))
    return ns_pr, ws_pr
end

# TEMP : at the moment, only supporting symm. quadratures
# # random
# gen_qset_ran(N::Int) = begin
#     rs = Arr{Fl,1}(undef, N)
#     while true
#         rs = -1. .+ 2. * rand(N)
#         # don't allow duplicate elements in rs
#         if length(unique(rs)) == length(rs)
#             break
#         end
#     end

#     ns = sort(rs)
#     es = 0.5 * (ns[1:(N-1)] .+ ns[2:N])
#     es_wb = [-1. ; es ; 1.] # include bounding edges
#     ws = [es_wb[i] - es_wb[i-1] for i in 2:(N+1)]

#     return ns, ws
# end

# NOTE : Axis Constants
# NOTE : start all @enum types at 1 (to keep consistent w/ Julia indexing)
@enum AX_ID begin
    AX_ID_I = 1
    AX_ID_J
    AX_ID_K
end

# NOTE : LQN quadrature set data
include("data/LQN.jl")
# NOTE : using standard LQN quad. term. for ``N''. $N (N+2)/8$ ordinates /
# octant, $N (N+2)$ ordinates / unit sphere
gen_sqset_lqn(N::Int) = begin
    valid =  ((MIN_LQN_ORDER <= N <= MAX_LQN_ORDER) &&
              iseven(N))
    if ! valid
        error()
    end

    lqn_set = lqn_sets[N]

    pjs   = lqn_set[:projs]
    indws = lqn_set[:weights]
    inds  = lqn_set[:inds]

    No = div(N * (N+2), 8) # no. ords. in octant

    μs = Arr{Fl,1}(undef, No)
    ηs = Arr{Fl,1}(undef, No)
    ξs = Arr{Fl,1}(undef, No)

    ws = Arr{Fl,1}(undef, No)
    # NOTE : loop structure consistent with libdetran, although indices are made
    # to be one-based (Julia vs. C++)

    m = 1
    for i in 1:(Int(N / 2))
        for j in 1:(Int(N / 2) - i + 1)
            μ = pjs[i]
            η = pjs[j]
            ξ = sqrt(1.0 - μ^2 - η^2)
            μs[m] = μ
            ηs[m] = η
            ξs[m] = ξ
            # NOTE : in detran, weights are normalized to one / octant, we
            # re-normalize over the unit sphere (4π)
            ws[m] = (pi / 2) * indws[inds[m]]
            m += 1
        end
    end

    return μs, ηs, ξs, ws
end

# uniform and equal weight quadrature set
# NOTE : N = N_{ξ}
gen_sqset_uen(N::Int; pol_ax_id::AX_ID=AX_ID_K) = begin
    valid = ( (N >= 2)  && iseven(N))
    if ! valid
        error()
    end

    Nrξ = N
    Nrω = 2 * Nrξ

    No =  div(N*N, 4) # no. ords. in octant
    # NOTE : rotated so that pol. angle w/ ξ
    rμs = Arr{Fl,1}(undef, No)
    rηs = Arr{Fl,1}(undef, No)
    rξs = Arr{Fl,1}(undef, No)

    # polar cosines
    pcs = [(2. / Nrξ) * (i - 0.5) for i in 1:div(Nrξ, 2)]
    ωs = [((2. * π) / Nrω) * (i - 0.5) for i in 1:div(Nrω, 4) ]

    m = 1
    for pc in pcs
        for ω in ωs
            rμs[m] = sqrt(1-pc^2) * cos(ω)
            rηs[m] = sqrt(1-pc^2) * sin(ω)
            rξs[m] = pc
            m += 1
        end
    end

    ws = fill((4 * π) / (Nrξ * Nrω), div(Nrξ*Nrξ, 4))

    if     pol_ax_id == AX_ID_I
        μs = rξs
        ηs = rμs
        ξs = rηs
    elseif pol_ax_id == AX_ID_J
        μs = rμs
        ηs = rξs
        ξs = rηs
    elseif pol_ax_id == AX_ID_K
        μs = rμs
        ηs = rηs
        ξs = rξs
    end

    return μs, ηs, ξs, ws
end

# NOTE : Angle Collections

abstract type AngColl end
AnCo = AngColl

# sphere (incl. all possible angles)
struct SpheColl <: AngColl end
SpCo = SpheColl
SC   = SpheColl()

# hemisphere
@enum HC_ID begin
    # NOTE : Stand
    HC_ID_R = 1 # (R)i ; (+i) ; (Principal)
    HC_ID_L     # (L)e ; (-i)
    # NOTE : Extra
    HC_ID_U     # (U)p ; (+j)
    HC_ID_D     # (D)o ; (-j)
    HC_ID_O     # (O)u ; (+k)
    HC_ID_I     # (I)n ; (-k)
end
PR_HC_ID = HC_ID_R
# TODO : find a way to get enum subtypes
STD_HC_IDS = [HC_ID_R, HC_ID_L]

struct HemiColl <: AngColl
    hc_id :: HC_ID
end
HeCo = HemiColl

get_hc_id(hc::HeCo) = hc.hc_id

PR_HC   = HeCo(PR_HC_ID)    # principal HemiColl
STD_HCS = HeCo.(STD_HC_IDS) # standard  HemiColl's

HC_R = HeCo(HC_ID_R)
HC_L = HeCo(HC_ID_L)
HC_U = HeCo(HC_ID_U)
HC_D = HeCo(HC_ID_D)
HC_O = HeCo(HC_ID_O)
HC_I = HeCo(HC_ID_I)

is_r(hc::HeCo) = (get_hc_id(hc) == HC_ID_R)
is_l(hc::HeCo) = (get_hc_id(hc) == HC_ID_L)
# TODO : is_u, is_d, is_i, ... for other HC_ID's

μ_si(hc::HeCo) = begin
    hc_id = get_hc_id(hc)
    if     hc_id == HC_ID_R
        si = + 1
    elseif hc_id == HC_ID_L
        si = - 1
    else
        error()
    end

    return si
end
# TODO : η_si, ξ_si for HeCO's

# quadrant
@enum QC_ID begin
    # NOTE : Stand
    QC_ID_RU = 1 # (R)i - (U)p ; (+i; +j) (Principal)
    QC_ID_LU     # (L)e - (U)p ; (-i; +j)
    QC_ID_RD     # (R)i - (D)o ; (+i, -j)
    QC_ID_LD     # (L)e - (D)o ; (-i, -j)
    # NOTE : Extra
    # TODO : add extra i.d's, such as QC_ID_UO, QC_ID_UI, ...
end
PR_QC_ID = QC_ID_RU
STD_QC_IDS = [QC_ID_RU, QC_ID_LU, QC_ID_RD, QC_ID_LD]

struct QuadColl <: AngColl
    qc_id :: QC_ID
end
QuCo = QuadColl

get_qc_id(qc::QuCo) = qc.qc_id

PR_QC   = QuCo(PR_QC_ID)    # principal QuadColl
STD_QCS = QuCo.(STD_QC_IDS) # standard  QuadColl's

QC_RU = QuCo(QC_ID_RU)
QC_LU = QuCo(QC_ID_LU)
QC_RD = QuCo(QC_ID_RD)
QC_LD = QuCo(QC_ID_LD)

is_ru(qc::QuCo) = (get_qc_id(qc) == QC_ID_RU)
is_lu(qc::QuCo) = (get_qc_id(qc) == QC_ID_LU)
is_rd(qc::QuCo) = (get_qc_id(qc) == QC_ID_RD)
is_ld(qc::QuCo) = (get_qc_id(qc) == QC_ID_LD)

is_r(qc::QuCo) = (is_ru(qc) || is_rd(qc))
is_l(qc::QuCo) = (is_lu(qc) || is_ld(qc))
is_u(qc::QuCo) = (is_ru(qc) || is_lu(qc))
is_d(qc::QuCo) = (is_rd(qc) || is_ld(qc))
# TODO : extra direction conditionals for QC's, is_i, is_o, ...

μ_si(qc::QuCo) = begin
    qc_id = get_qc_id(qc)
    if     qc_id == QC_ID_RU
        si = + 1
    elseif qc_id == QC_ID_LU
        si = - 1
    elseif qc_id == QC_ID_RD
        si = + 1
    elseif qc_id == QC_ID_LD
        si = - 1
    end

    return si
end
η_si(qc::QuCo) = begin
    qc_id = get_qc_id(qc)
    if     qc_id == QC_ID_RU
        si = + 1
    elseif qc_id == QC_ID_LU
        si = + 1
    elseif qc_id == QC_ID_RD
        si = - 1
    elseif qc_id == QC_ID_LD
        si = - 1
    end

    return si
end
# TODO : ξ_si for QC's

# # octant
@enum OC_ID begin
    # NOTE : Stand
    OC_ID_RUO  = 1 # (R)i - (U)p - (O)u ; (+i, +j, +k) (Principal)
    OC_ID_LUO      # (L)e - (U)p - (O)u ; (-i, +j, +k)
    OC_ID_RDO      # (R)i - (D)o - (O)u ; (+i, -j, +k)
    OC_ID_LDO      # (L)e - (D)o - (O)u ; (-i, -j, +k)
    OC_ID_RUI      # (R)i - (U)p - (I)n ; (+i, +j, -k)
    OC_ID_LUI      # (L)e - (U)p - (I)n ; (-i, +j, -k)
    OC_ID_RDI      # (R)i - (D)o - (I)n ; (+i, -j, -k)
    OC_ID_LDI      # (L)e - (D)o - (I)n ; (-i, -j, -k)
end
PR_OC_ID = OC_ID_RUO
STD_OC_IDS = [OC_ID_RUO, OC_ID_LUO, OC_ID_RDO, OC_ID_LDO,
              OC_ID_RUI, OC_ID_LUI, OC_ID_RDI, OC_ID_LDI]

struct OctaColl <: AngColl
    oc_id :: OC_ID
end
OcCo = OctaColl

get_oc_id(oc::OcCo) = oc.oc_id

PR_OC   = OcCo(PR_OC_ID)    # principal OctaColl
STD_OCS = OcCo.(STD_OC_IDS) # standard  OctaColl's

OC_RUO = OcCo(OC_ID_RUO)
OC_LUO = OcCo(OC_ID_LUO)
OC_RDO = OcCo(OC_ID_RDO)
OC_LDO = OcCo(OC_ID_LDO)
OC_RUI = OcCo(OC_ID_RUI)
OC_LUI = OcCo(OC_ID_LUI)
OC_RDI = OcCo(OC_ID_RDI)
OC_LDI = OcCo(OC_ID_LDI)

is_ruo(oc::OcCo) = (get_oc_id(oc) == OC_ID_RUO)
is_luo(oc::OcCo) = (get_oc_id(oc) == OC_ID_LUO)
is_rdo(oc::OcCo) = (get_oc_id(oc) == OC_ID_RDO)
is_ldo(oc::OcCo) = (get_oc_id(oc) == OC_ID_LDO)
is_rui(oc::OcCo) = (get_oc_id(oc) == OC_ID_RUI)
is_lui(oc::OcCo) = (get_oc_id(oc) == OC_ID_LUI)
is_rdi(oc::OcCo) = (get_oc_id(oc) == OC_ID_RDI)
is_ldi(oc::OcCo) = (get_oc_id(oc) == OC_ID_LDI)

is_ru(oc::OcCo) = (is_ruo(oc) || is_rui(oc))
is_lu(oc::OcCo) = (is_luo(oc) || is_lui(oc))
is_rd(oc::OcCo) = (is_rdo(oc) || is_rdi(oc))
is_ld(oc::OcCo) = (is_ldo(oc) || is_ldi(oc))

is_r(oc::OcCo) = (is_ruo(oc) || is_rdo(oc) || is_rui(oc) || is_rdi(oc))
is_l(oc::OcCo) = (is_luo(oc) || is_ldo(oc) || is_lui(oc) || is_ldi(oc))
is_u(oc::OcCo) = (is_ruo(oc) || is_luo(oc) || is_rui(oc) || is_lui(oc))
is_d(oc::OcCo) = (is_rdo(oc) || is_ldo(oc) || is_rdi(oc) || is_ldi(oc))
is_o(oc::OcCo) = (is_ruo(oc) || is_luo(oc) || is_rdo(oc) || is_ldo(oc))
is_i(oc::OcCo) = (is_rui(oc) || is_lui(oc) || is_rdi(oc) || is_ldi(oc))

μ_si(oc::OcCo) = begin
    oc_id = get_oc_id(oc)
    if     oc_id == OC_ID_RUO
        si = + 1
    elseif oc_id == OC_ID_LUO
        si = - 1
    elseif oc_id == OC_ID_RDO
        si = + 1
    elseif oc_id == OC_ID_LDO
        si = - 1
    elseif oc_id == OC_ID_RUI
        si = + 1
    elseif oc_id == OC_ID_LUI
        si = - 1
    elseif oc_id == OC_ID_RDI
        si = + 1
    elseif oc_id == OC_ID_LDI
        si = - 1
    end

    return si
end
η_si(oc::OcCo) = begin
    oc_id = get_oc_id(oc)
    if     oc_id == OC_ID_RUO
        si = + 1
    elseif oc_id == OC_ID_LUO
        si = + 1
    elseif oc_id == OC_ID_RDO
        si = - 1
    elseif oc_id == OC_ID_LDO
        si = - 1
    elseif oc_id == OC_ID_RUI
        si = + 1
    elseif oc_id == OC_ID_LUI
        si = + 1
    elseif oc_id == OC_ID_RDI
        si = - 1
    elseif oc_id == OC_ID_LDI
        si = - 1
    end

    return si
end
ξ_si(oc::OcCo) = begin
    oc_id = get_oc_id(oc)
    if     oc_id == OC_ID_RUO
        si = + 1
    elseif oc_id == OC_ID_LUO
        si = + 1
    elseif oc_id == OC_ID_RDO
        si = + 1
    elseif oc_id == OC_ID_LDO
        si = + 1
    elseif oc_id == OC_ID_RUI
        si = - 1
    elseif oc_id == OC_ID_LUI
        si = - 1
    elseif oc_id == OC_ID_RDI
        si = - 1
    elseif oc_id == OC_ID_LDI
        si = - 1
    end

    return si
end

# union of QuadColl and OctaColl types
QOCo = Union{QuCo, OcCo}
# union of SpheColl and HemiColl types
SHCo = Union{SpCo, HeCo}

# TODO : multi-angle

# single-angle
struct SingColl <: AngColl
    n :: Int
    SingColl(n) = (n > 0) ? new(n) : error()
end
SiCo = SingColl

get_n(sc::SiCo) = sc.n

# NOTE : utility funs to split an ang. coll. into smaller ang sub coll.'s
# TODO : implement others
split_SpCo_HeCo(sc::SpCo) = STD_HCS

split_QuCo_OcCo(qc::QuCo) = begin
    qc_id = get_qc_id(qc)
    if     qc_id == QC_ID_RU
        ocs = [OC_RUO, OC_RUI]
    elseif qc_id == QC_ID_LU
        ocs = [OC_LUO, OC_LUI]
    elseif qc_id == QC_ID_RD
        ocs = [OC_RDO, OC_RDI]
    elseif qc_id == QC_ID_LD
        ocs = [OC_LDO, OC_LDI]
    else
        error()
    end

    return ocs
end

split_HeCo_OcCo(hc::HeCo) = begin
    hc_id = get_hc_id(hc)
    if     hc_id == HC_ID_R
        ocs = [OC_RUO, OC_RDO, OC_RUI, OC_RDI]
    elseif hc_id == HC_ID_L
        ocs = [OC_LUO, OC_LDO, OC_LUI, OC_LDI]
    elseif hc_id == HC_ID_U
        ocs = [OC_RUO, OC_LUO, OC_RUI, OC_LUI]
    elseif hc_id == HC_ID_D
        ocs = [OC_RDO, OC_LDO, OC_RDI, OC_LDI]
    elseif hc_id == HC_ID_O
        ocs = [OC_RUO, OC_LUO, OC_RDO, OC_LDO]
    elseif hc_id == HC_ID_I
        ocs = [OC_RUI, OC_LUI, OC_RDI, OC_LDI]
    else
        error()
    end

    return ocs
end

# NOTE : Angular Quadrature

abstract type AngQuad end
AnQu = AngQuad
abstract type PolQuad <: AngQuad end
PoQu = PolQuad
abstract type FulQuad <: AngQuad end
FuQu = FulQuad

struct SymPolQuad <: PolQuad
    N  :: Int
    μs :: Arr{Fl,1}
    ws :: Arr{Fl,1}
    SymPolQuad(N, μs_pr, ws_pr) = begin
        N_h_μs = length(μs_pr)
        N_h_ws = length(ws_pr)

        valid = ((N > 0)                                 &&
                 (mod(N,2) == 0)                         &&
                 (div(N,2) == N_h_μs == N_h_ws)          &&
                 all(Int.(sign.(μs_pr)) .== μ_si(PR_HC))   )

        if ! valid
            error()
        end

        μs = vcat([μ_si(hc) * μs_pr for hc in STD_HCS]...)
        ws = vcat([           ws_pr for hc in STD_HCS]...)

        return new(N, μs, ws)
    end
end
SyPoQu = SymPolQuad

get_N(spq::SyPoQu)   = spq.N
get_N_h(spq::SyPoQu) = div(get_N(spq), 2)
# no. angles in AngColl derived type
get_N_ac(spq::SyPoQu, sc::SpCo) = get_N(spq)
get_N_ac(spq::SyPoQu, hc::HeCo) = get_N_h(spq)

get_n_hc_off(spq::SyPoQu, hc::HeCo) = begin
    n_hc_off = (Int(get_hc_id(hc)) - 1)*get_N_h(spq)
    return n_hc_off
end

get_ns(spq::SyPoQu) = 1:get_N(spq)
get_ns(spq::SyPoQu, sc::SpCo) = get_ns(spq)
get_ns(spq::SyPoQu, hc::HeCo) = begin
    n_hc_off = get_n_hc_off(spq, hc)
    ns = n_hc_off .+ (1:get_N_h(spq))
    return ns
end

get_μs(spq::SyPoQu) = spq.μs[get_ns(spq)]
get_ws(spq::SyPoQu) = spq.ws[get_ns(spq)]

get_μs(spq::SyPoQu, sc::SpCo) = spq.μs[get_ns(spq, sc)]
get_ws(spq::SyPoQu, sc::SpCo) = spq.ws[get_ns(spq, sc)]

get_μs(spq::SyPoQu, hc::HeCo) = spq.μs[get_ns(spq, hc)]
get_ws(spq::SyPoQu, hc::HeCo) = spq.ws[get_ns(spq, hc)]

get_μs(spq::SyPoQu, ns) = spq.μs[ns]
get_ws(spq::SyPoQu, ns) = spq.ws[ns]

struct SymFulQuad <: FulQuad
    N  :: Int # total no. ordinates (NOTE : different than standard S_{N} term.)
    μs :: Arr{Fl,1}
    ηs :: Arr{Fl,1}
    ξs :: Arr{Fl,1}
    ws :: Arr{Fl,1}
    SymFulQuad(N, μs_pr, ηs_pr, ξs_pr, ws_pr) = begin
        N_o_μs = length(μs_pr) # no. in octant
        N_o_ηs = length(ηs_pr)
        N_o_ξs = length(ξs_pr)
        N_o_ws = length(ws_pr)

        valid = ((N > 0)                                            &&
                 (mod(N,8) == 0)                                    &&
                 (div(N,8) == N_o_μs == N_o_ηs == N_o_ξs == N_o_ws) &&
                 all(Int.(sign.(μs_pr)) .== μ_si(PR_OC))            &&
                 all(Int.(sign.(ηs_pr)) .== η_si(PR_OC))            &&
                 all(Int.(sign.(ξs_pr)) .== ξ_si(PR_OC))              )

        if ! valid
            error()
        end

        μs = vcat([μ_si(oc) * μs_pr for oc in STD_OCS]...)
        ηs = vcat([η_si(oc) * ηs_pr for oc in STD_OCS]...)
        ξs = vcat([ξ_si(oc) * ξs_pr for oc in STD_OCS]...)
        ws = vcat([           ws_pr for oc in STD_OCS]...)

        return new(N, μs, ηs, ξs, ws)
    end
end
SyFuQu = SymFulQuad

get_N(sfq::SyFuQu) = sfq.N
get_N_h(sfq::SyFuQu) = div(get_N(sfq), 2)
get_N_q(sfq::SyFuQu) = div(get_N(sfq), 4)
get_N_o(sfq::SyFuQu) = div(get_N(sfq), 8)
# no. angles in AngColl derived type
get_N_ac(sfq::SyFuQu, sc::SpCo) = get_N(sfq)
# TODO : get_N_ac(..., [HeCo, QuCo])
# get_N_ac(sfq::SyFuQu, hc::HeCo) = get_N_h(sfq)
# get_N_ac(sfq::SyFuQu, qc::QuCo) = get_N_q(sfq)
get_N_ac(sfq::SyFuQu, oc::OcCo) = get_N_o(sfq)

# TODO : get_N_hc_off, get_N_qc_off
get_n_oc_off(sfq::SyFuQu, oc::OcCo) = begin
    n_oc_off = (Int(get_oc_id(oc)) - 1)*get_N_o(sfq)
    return n_oc_off
end

get_ns(sfq::SyFuQu) = 1:get_N(sfq)
get_ns(sfq::SyFuQu, sc::SpCo) = get_ns(sfq)
get_ns(sfq::SyFuQu, oc::OcCo) = begin
    n_oc_off = get_n_oc_off(sfq, oc)
    ns = n_oc_off .+ (1:get_N_o(sfq))
    return ns
end
get_ns(sfq::SyFuQu, qc::QuCo) = begin
    ns = vcat([get_ns(sfq, oc) for oc in split_QuCo_OcCo(qc)]...)
    return ns
end
get_ns(sfq::SyFuQu, hc::HeCo) = begin
    ns = vcat([get_ns(sfq, oc) for oc in split_HeCo_OcCo(hc)]...)
    return ns
end

get_μs(sfq::SyFuQu) = sfq.μs[get_ns(sfq)]
get_ηs(sfq::SyFuQu) = sfq.ηs[get_ns(sfq)]
get_ξs(sfq::SyFuQu) = sfq.ξs[get_ns(sfq)]
get_ws(sfq::SyFuQu) = sfq.ws[get_ns(sfq)]

get_μs(sfq::SyFuQu, ns) = sfq.μs[ns]
get_ηs(sfq::SyFuQu, ns) = sfq.ηs[ns]
get_ξs(sfq::SyFuQu, ns) = sfq.ξs[ns]
get_ws(sfq::SyFuQu, ns) = sfq.ws[ns]

# TODO : collapse all these getters (once general get_ns are implem. for each
# AngColl type)
get_μs(sfq::SyFuQu, ac::AnCo) = sfq.μs[get_ns(sfq, ac)]
get_ηs(sfq::SyFuQu, ac::AnCo) = sfq.ηs[get_ns(sfq, ac)]
get_ξs(sfq::SyFuQu, ac::AnCo) = sfq.ξs[get_ns(sfq, ac)]
get_ws(sfq::SyFuQu, ac::AnCo) = sfq.ws[get_ns(sfq, ac)]

struct GenPolQuad <: PolQuad end
GePoQu = GenPolQuad
struct GenFulQuad <: FulQuad end
GeFuQu = GenFulQuad

# NOTE : Mesh

abstract type Mesh end
Me = Mesh

abstract type StrucMesh <: Mesh end
StMe = StrucMesh

abstract type UnstrMesh <: Mesh end
UnMe = UnstrMesh

abstract type CartMesh <: StrucMesh end
CaMe = CartMesh

struct SlabMesh <: CartMesh
    dxs :: Arr{Fl,1}
    I   :: Int
    SlabMesh(dxs, I) = begin
        valid = ((I > 0)            &&
                 (I == length(dxs)) &&
                 all(dxs .> 0)        )
        if ! valid
            error()
        end

        return new(dxs, I)
    end
end
SlMe = SlabMesh

SlabMesh(dx::Number, I) = SlabMesh(fill(dx, I), I)
SlabMesh(dxs) = SlabMesh(dxs, length(dxs))

get_I(sm::SlMe)   = sm.I            # no. i      cell indices
get_H(sm::SlMe)   = get_I(sm)       # no. linear cell indices
get_H_f(sm::SlMe) = (get_H(sm) + 1) # no. linear face indices

# get stencil size
get_sten_size(sm::SlMe) = 3

get_hs(sm::SlMe)   = 1:get_H(sm)
get_h_fs(sm::SlMe) = 1:get_H_f(sm)

is_valid_h(sm::SlMe, h::Int) = (0 < h < (get_H(sm) + 1))
is_valid_h_f(sm::SlMe, h_f::Int) = (0 < h_f < (get_H_f(sm) + 1))

# grab internal face indices only (excluding boundaries)
get_h_f_ints(sm::SlMe) = 2:(get_H_f(sm) - 1)

get_lis(sm::SlMe) = LinearIndices((get_I(sm),))
get_cis(sm::SlMe) = CartesianIndices((get_I(sm),))

get_li(sm::SlMe, i::Int) = get_lis(sm)[i]
get_lis(sm::SlMe, is) = get_lis(sm)[is]

get_ci(sm::SlMe, h::Int) = get_cis(sm)[h]
get_cis(sm::SlMe, hs) = get_cis(sm)[hs]

# get right cell linear indices
get_h_r(sm::SlMe, h::Int) = begin
    i, = get_ci(sm, h).I
    i_r = i + 1
    h_r = get_li(sm, i_r)
    return h_r
end
get_h_rs(sm::SlMe, h::Int) = get_h_r(sm, h)
get_h_rs(sm::SlMe, hs) = [get_h_r(sm, h) for h in hs]

# get left cell linear indices
get_h_l(sm::SlMe, h::Int) = begin
    i, = get_ci(sm, h).I
    i_l = i - 1
    h_l = get_li(sm, i_l)
    return h_l
end
get_h_ls(sm::SlMe, h::Int) = get_h_l(sm, h)
get_h_ls(sm::SlMe, hs) = [get_h_l(sm, h) for h in hs]

is_h_f_rl(sm::SlMe, h_f::Int) = begin
    if ! is_valid_h_f(sm, h_f)
        error()
    end
    # NOTE : all slab mesh faces are right-left (i.e) have normal +/- i
    return true
end

# given face index, get linear cell index (positive orientation) (+i)
get_h_f_h_p(sm::SlMe, h_f::Int) = begin
    return (0 < h_f < get_H_f(sm)) ? h_f : error()
end
# given face index, get linear cell index (negative orientation) (-i)
get_h_f_h_m(sm::SlMe, h_f::Int) = begin
    return (1 < h_f <= get_H_f(sm)) ? (h_f - 1) : error()
end

# get right- and left-face linear indices (useful for face-based disc.)
get_h_f_r(sm::SlMe , h::Int) = begin
    i, = get_ci(sm, h).I
    h_f_r = i + 1
    return h_f_r
end
get_h_f_rs(sm::SlMe, h::Int) = get_h_f_r(sm, h)
get_h_f_rs(sm::SlMe, hs) = [get_h_f_r(sm, h) for h in hs]

get_h_f_l(sm::SlMe , h::Int) = begin
    i, = get_ci(sm, h).I
    h_f_l = i
    return h_f_l
end
get_h_f_ls(sm::SlMe, h::Int) = get_h_f_l(sm, h)
get_h_f_ls(sm::SlMe, hs) = [get_h_f_l(sm, h) for h in hs]

# NOTE : accessed by LINEAR INDEXING (default)
get_dx(sm::SlMe, h::Int)  = sm.dxs[h]
get_dxs(sm::SlMe)         = sm.dxs
get_dxs(sm::SlMe, hs)     = sm.dxs[hs]

# pseudo-volume (cell)
get_dpv(sm::SlMe, h::Int) = sm.dxs[h]
get_dpvs(sm::SlMe)        = sm.dxs
get_dpvs(sm::SlMe, hs)    = sm.dxs[hs]
# pseudo-area (face)
get_dpa(sm::SlMe, h_f::Int) = 1.

# NOTE : accessed by CARTESIAN INDEXING
c_get_dxs(sm::SlMe, is) = sm.dxs[get_lis(sm, is)]

# get (in)coming and (ou)tgoing (r)ight-(l)eft face indices
get_h_f_in_rls(pq::PoQu, sm::SlMe, hc::HeCo, hs) = begin
    if     is_r(hc)
        get_h_f_ls(sm, hs)
    elseif is_l(hc)
        get_h_f_rs(sm, hs)
    end
end
get_h_f_ou_rls(pq::PoQu, sm::SlMe, hc::HeCo, hs) = begin
    if     is_r(hc)
        get_h_f_rs(sm, hs)
    elseif is_l(hc)
        get_h_f_ls(sm, hs)
    end
end

# get boundary face indices
get_h_f_rbs(sm::SlMe) = [get_H_f(sm)] # right bound.
get_h_f_lbs(sm::SlMe) = [1] # left bound.
get_h_f_bs(sm::SlMe)  = [get_h_f_rbs(sm); get_h_f_lbs(sm)] # all bound.

# get incoming / outgoing boundary face indices (for a spec. angle collection)
get_h_f_ibs(pq::PoQu, sm::SlMe, hc::HeCo) = begin
    if     is_r(hc)
        get_h_f_lbs(sm)
    elseif is_l(hc)
        get_h_f_rbs(sm)
    end
end
get_h_f_obs(pq::PoQu, sm::SlMe, hc::HeCo) = begin
    if     is_r(hc)
        get_h_f_rbs(sm)
    elseif is_l(hc)
        get_h_f_lbs(sm)
    end
end

# given a boundary face index, return its attached linear cell index
get_h_in(sm::SlMe, h_f::Int) = begin
    (h_f == 1) ? 1 : ((h_f == get_H_f(sm)) ? get_H(sm) : error())
end

struct RectMesh <: CartMesh
    dxs :: Arr{Fl,1}
    dys :: Arr{Fl,1}
    I   :: Int
    J   :: Int

    RectMesh(i_dxs, j_dys, I, J) = begin
        valid = ((I > 0)              && (J > 0)              &&
                 (I == length(i_dxs)) && (J == length(j_dys)) &&
                 all(i_dxs .> 0)      && all(j_dys .> 0)        )
        if ! valid
            error()
        end

        dxs = repeat(i_dxs, outer=J)
        dys = repeat(j_dys, inner=I)

        return new(dxs, dys, I, J)
    end
end
ReMe = RectMesh

RectMesh(dx::Number, dy::Number, I, J) = RectMesh(fill(dx, I), fill(dy, J), I, J)
RectMesh(dxs, dys) = RectMesh(dxs, dys, length(dxs), length(dys))

get_I(rm::ReMe) = rm.I
get_J(rm::ReMe) = rm.J
get_H(rm::ReMe) = get_I(rm)*get_J(rm)
get_H_f(rm::ReMe) = begin
    I, J = get_I(rm), get_J(rm)
    H_f = 2*I*J + I + J
    return H_f
end

get_sten_size(rm::ReMe) = 5

get_hs(rm::ReMe) = 1:get_H(rm)
get_h_fs(rm::ReMe) = 1:get_H_f(rm)

is_valid_h(rm::ReMe, h::Int) = (0 < h < (get_H(rm) + 1))
is_valid_h_f(rm::ReMe, h_f::Int) = (0 < h_f < (get_H_f(rm) + 1))

# TODO : decide if these should be in any particular order
# TODO : make slab version of this function similar
get_h_f_ints(rm::ReMe) = begin
    h_fs   = get_h_fs(rm)
    h_f_bs = get_h_f_bs(rm)
    h_f_ints = setdiff(h_fs, h_f_bs)
    return h_f_ints
end

get_lis(rm::ReMe) = LinearIndices((get_I(rm), get_J(rm)))
get_cis(rm::ReMe) = CartesianIndices((get_I(rm), get_J(rm)))

get_li(rm::ReMe, i::Int, j::Int) = get_lis(rm)[i,j]
get_lis(rm::ReMe, is, js) = get_lis(rm)[is, js]

get_ci(rm::ReMe, h::Int) = get_cis(rm)[h]
get_cis(rm::ReMe, hs) = get_cis(rm)[hs]

# TODO : adjacent cell functions could be made MUCH MORE EFFICIENT (at the
# moment, them are written for clarity, w/ explicit index conversion)
get_h_r(rm::ReMe, h::Int) = begin
    i, j = get_ci(rm, h).I
    i_r, j_r = i + 1, j
    h_r = get_li(rm, i_r, j_r)
    return h_r
end
get_h_rs(rm::ReMe, h::Int) = get_h_r(rm, h)
get_h_rs(rm::ReMe, hs) = [get_h_r(rm, h) for h in hs]

get_h_l(rm::ReMe, h::Int) = begin
    i, j = get_ci(rm, h).I
    i_l, j_l = i - 1, j
    h_l = get_li(rm, i_l, j_l)
    return h_l
end
get_h_ls(rm::ReMe, h::Int) = get_h_l(rm, h)
get_h_ls(rm::ReMe, hs) = [get_h_l(rm, h) for h in hs]

get_h_u(rm::ReMe, h::Int) = begin
    i, j = get_ci(rm, h).I
    i_u, j_u = i, j + 1
    h_u = get_li(rm, i_u, j_u)
    return h_u
end
get_h_us(rm::ReMe, h::Int) = get_h_u(rm, h)
get_h_us(rm::ReMe, hs) = [get_h_u(rm, h) for h in hs]

get_h_d(rm::ReMe, h::Int) = begin
    i, j = get_ci(rm, h).I
    i_d, j_d = i, j - 1
    h_d = get_li(rm, i_d, j_d)
    return h_d
end
get_h_ds(rm::ReMe, h::Int) = get_h_d(rm, h)
get_h_ds(rm::ReMe, hs) = [get_h_d(rm, h) for h in hs]

# face has normals +/- i
is_h_f_rl(rm::ReMe, h_f::Int) = begin
    if ! is_valid_h_f(rm, h_f)
        error()
    end
    off_rl = get_off_rl(rm)
    return (0 < h_f < (off_rl + 1))
end
# face has normals +/- j
is_h_f_ud(rm::ReMe, h_f::Int) = begin
    if ! is_valid_h_f(rm, h_f)
        error()
    end
    H_f = get_H_f(rm)
    off_rl = get_off_rl(rm)
    return (off_rl < h_f < (H_f + 1))
end

# given face index, get linear cell index (positive orientation)
# TODO : D.R.Y. up get_h_f_h_p and get_h_f_h_m
# TODO : vectorize / remove if unnec. if cond.
get_h_f_h_p(rm::ReMe, h_f::Int) = begin
    if ! is_valid_h_f(rm, h_f)
        error()
    end

    I, J = get_I(rm), get_J(rm)
    H_f  = get_H_f(rm)
    off_rl = get_off_rl(rm)

    if (h_f > off_rl)
        # face normal +/- j
        # NOTE : one-based indexing is dumb
        i = div(h_f-off_rl-1, J+1) + 1
        j_u = h_f - off_rl - (i-1)*(J+1)
        # NOTE : will throw if j_u is beyond J
        h_u = get_li(rm, i, j_u)
        h_p = h_u
    else
        # face normal +/- i
        j   = div(h_f-1, I+1) + 1
        i_r = h_f - (j-1)*(I+1)
        # NOTE : will throw if i_r is beyond I
        h_r = get_li(rm, i_r, j)
        h_p = h_r
    end

    return h_p
end
get_h_f_h_m(rm::ReMe, h_f::Int) = begin
    if ! is_valid_h_f(rm, h_f)
        error()
    end

    I, J = get_I(rm), get_J(rm)
    H_f  = get_H_f(rm)
    off_rl = get_off_rl(rm)

    if (h_f > off_rl)
        # face normal +/- j
        i = div(h_f-off_rl-1, J+1) + 1
        j_d = h_f - off_rl - (i-1)*(J+1) - 1
        h_d = get_li(rm, i, j_d)
        h_m = h_d
    else
        # face normal +/- i
        j   = div(h_f-1, I+1) + 1
        i_l = h_f - (j-1)*(I+1) - 1
        h_l = get_li(rm, i_l, j)
        h_m = h_l
    end

    return h_m
end

get_h_f_r(rm::ReMe, h::Int) = begin
    I = get_I(rm)
    i, j = get_ci(rm, h).I
    off  = (j-1)*(I+1)
    h_f_r = off + (i + 1)
    return h_f_r
end
get_h_f_rs(rm::ReMe, h::Int) = get_h_f_r(rm, h)
get_h_f_rs(rm::ReMe, hs) = [get_h_f_r(rm, h) for h in hs]

get_h_f_l(rm::ReMe, h::Int) = begin
    I = get_I(rm)
    i, j = get_ci(rm, h).I
    off  = (j-1)*(I+1)
    h_f_l = off + i
    return h_f_l
end
get_h_f_ls(rm::ReMe, h::Int) = get_h_f_l(rm, h)
get_h_f_ls(rm::ReMe, hs) = [get_h_f_l(rm, h) for h in hs]

# helper function
get_off_rl(rm::ReMe) = begin
    I, J = get_I(rm), get_J(rm)
    return J*(I+1)
end

get_off_ud(rm::ReMe, h::Int) = begin
    I, J   = get_I(rm), get_J(rm)
    i, j   = get_ci(rm, h).I
    off_rl = get_off_rl(rm)
    off_i  = (i-1)*(J+1)
    off_ud = off_rl + off_i
    return off_ud
end
get_h_f_u(rm::ReMe, h::Int) = begin
    i, j    = get_ci(rm, h).I
    off_ud  = get_off_ud(rm, h)
    h_f_u = off_ud + (j + 1)
    return h_f_u
end
get_h_f_us(rm::ReMe, h::Int) = get_h_f_u(rm, h)
get_h_f_us(rm::ReMe, hs) = [get_h_f_u(rm, h) for h in hs]

get_h_f_d(rm::ReMe, h::Int) = begin
    i, j    = get_ci(rm, h).I
    off_ud  = get_off_ud(rm, h)
    h_f_d = off_ud + j
    return h_f_d
end
get_h_f_ds(rm::ReMe, h::Int) = get_h_f_d(rm, h)
get_h_f_ds(rm::ReMe, hs) = [get_h_f_d(rm, h) for h in hs]

get_dx(rm::ReMe, h::Int) = rm.dxs[h]
get_dxs(rm::ReMe)        = rm.dxs
get_dxs(rm::ReMe, hs)    = rm.dxs[hs]

get_dy(rm::ReMe, h::Int) = rm.dys[h]
get_dys(rm::ReMe)        = rm.dys
get_dys(rm::ReMe, hs)    = rm.dys[hs]

# pseudo-volume (cell)
get_dpv(rm::ReMe, h::Int) = rm.dxs[h]*rm.dys[h]
get_dpvs(rm::ReMe)        = rm.dxs .* rm.dys
get_dpvs(rm::ReMe, hs)    = rm.dxs[hs] .* rm.dys[hs]
# pseudo-area (face)
# TODO : slow, ugly!
get_dpa(rm::ReMe, h_f::Int) = begin
    if h_f in get_h_f_bs(rm)
        h = get_h_in(rm, h_f)
    else
        # NOTE : h_p and h_m should have the same dpa :o for contig. meshes
        h = get_h_f_h_p(rm, h_f)
    end

    if     is_h_f_rl(rm, h_f)
        dpa = get_dy(rm, h)
    elseif is_h_f_ud(rm, h_f)
        dpa = get_dx(rm, h)
    end

    return dpa
end

# # NOTE : accessed by CARTESIAN INDEXING
c_get_dxs(rm::ReMe, is, js) = rm.dxs[get_lis(rm, is, js)]
c_get_dys(rm::ReMe, is, js) = rm.dys[get_lis(rm, is, js)]

# get (in)coming and (ou)tgoing (r)ight-(l)eft face indices
get_h_f_in_rls(fq::FuQu, rm::ReMe, qoc::QOCo, hs) = begin
    if     is_r(qoc)
        get_h_f_ls(rm, hs)
    elseif is_l(qoc)
        get_h_f_rs(rm, hs)
    end
end
get_h_f_ou_rls(fq::FuQu, rm::ReMe, qoc::QOCo, hs) = begin
    if     is_r(qoc)
        get_h_f_rs(rm, hs)
    elseif is_l(qoc)
        get_h_f_ls(rm, hs)
    end
end

# get (in)coming and (ou)tgoing (u)p-(d)own face indices
get_h_f_in_uds(fq::FuQu, rm::ReMe, qoc::QOCo, hs) = begin
    if is_u(qoc)
        get_h_f_ds(rm, hs)
    elseif is_d(qoc)
        get_h_f_us(rm, hs)
    end
end
get_h_f_ou_uds(fq::FuQu, rm::ReMe, qoc::QOCo, hs) = begin
    if     is_u(qoc)
        get_h_f_us(rm, hs)
    elseif is_d(qoc)
        get_h_f_ds(rm, hs)
    end
end

# get boundary face indices
get_h_f_rbs(rm::ReMe) = begin
    I, J = get_I(rm), get_J(rm)
    off = (I + 1)
    st = off
    h_f_rbs = st:off:(st+(J-1)*off)
    return h_f_rbs
end
get_h_f_lbs(rm::ReMe) = begin
    I, J = get_I(rm), get_J(rm)
    off = (I + 1)
    st  = 1
    h_f_lbs = st:off:(st+(J-1)*off)
    return h_f_lbs
end
get_h_f_ubs(rm::ReMe) = begin
    I, J = get_I(rm), get_J(rm)
    off_rl = J*(I+1)
    off_ud = J+1
    st = off_rl + off_ud
    h_f_ubs = st:off_ud:(st+(I-1)*off_ud)
end
get_h_f_dbs(rm::ReMe) = begin
    I, J = get_I(rm), get_J(rm)
    off_rl = J*(I+1)
    off_ud = J+1
    st = off_rl + 1
    h_f_dbs = st:off_ud:(st+(I-1)*off_ud)
end
get_h_f_bs(rm::ReMe) = begin
    h_f_bs = [get_h_f_rbs(rm);
              get_h_f_lbs(rm);
              get_h_f_ubs(rm);
              get_h_f_dbs(rm)]
    return h_f_bs
end

get_h_f_ibs(fq::FuQu, rm::ReMe, qoc::QOCo) = begin
    if     is_ru(qoc)
        [get_h_f_lbs(rm); get_h_f_dbs(rm)]
    elseif is_lu(qoc)
        [get_h_f_rbs(rm); get_h_f_dbs(rm)]
    elseif is_rd(qoc)
        [get_h_f_lbs(rm); get_h_f_ubs(rm)]
    elseif is_ld(qoc)
        [get_h_f_rbs(rm); get_h_f_ubs(rm)]
    end
end
get_h_f_obs(fq::FuQu, rm::ReMe, qoc::QOCo) = begin
    if     is_ru(qoc)
        [get_h_f_rbs(rm); get_h_f_ubs(rm)]
    elseif is_lu(qoc)
        [get_h_f_lbs(rm); get_h_f_ubs(rm)]
    elseif is_rd(qoc)
        [get_h_f_rbs(rm); get_h_f_dbs(rm)]
    elseif is_ld(qoc)
        [get_h_f_lbs(rm); get_h_f_dbs(rm)]
    end
end

# TODO : ugly, and slow. Consider adding get_h_f_h_r, get_h_f_h_u, ... functions
get_h_in(rm::ReMe, h_f::Int) = begin
    if     (h_f in get_h_f_rbs(rm)) || (h_f in get_h_f_ubs(rm))
        h_in = get_h_f_h_m(rm, h_f)
    elseif (h_f in get_h_f_lbs(rm)) || (h_f in get_h_f_dbs(rm))
        h_in = get_h_f_h_p(rm, h_f)
    else
        error()
    end

    return h_in
end

# TODO : implement
struct CubeMesh <: CartMesh end
CuMe = CubeMesh

# NOTE : Submesh Definitions

abstract type Submesh end
Su = Submesh

abstract type StrucSubmesh <: Submesh end
StSu = StrucSubmesh

abstract type SlabSubmesh <: StrucSubmesh end
SlSu = SlabSubmesh
abstract type RectSubmesh <: StrucSubmesh end
ReSu = RectSubmesh
abstract type CubeSubmesh <: StrucSubmesh end
CuSu = CubeSubmesh

struct FullSlabSubmesh <: SlabSubmesh end
FuSlSu = FullSlabSubmesh

valid_submesh(sm::SlMe, fss::FuSlSu) = true

get_i_r(sm::SlMe, fss::FuSlSu) = get_I(sm)
get_i_l(sm::SlMe, fss::FuSlSu) = 1

get_h_f_bs(sm::SlMe, fss::FuSlSu) = get_h_f_bs(sm)

struct ContSlabSubmesh <: SlabSubmesh
    i_r::Int
    i_l::Int
    ContSlabSubmesh(i_r, i_l) = begin
        valid = ((i_l > 0)    &&
                 (i_r >= i_l)   )
        if ! valid
            error()
        end

        return new(i_r, i_l)
    end
end
CoSlSu = ContSlabSubmesh

get_i_r(css::CoSlSu) = css.i_r
get_i_l(css::CoSlSu) = css.i_l

valid_submesh(sm::SlMe, css::CoSlSu) = begin
    I = get_I(sm)
    i_r, i_l = get_i_r(css), get_i_l(css)
    valid = (i_r <= I) && (i_l <= I)
    return valid
end

# TODO : use of valid_submesh may be ineff.
get_i_r(sm::SlMe, css::CoSlSu) = valid_submesh(sm, css) ? get_i_r(css) : error()
get_i_l(sm::SlMe, css::CoSlSu) = valid_submesh(sm, css) ? get_i_l(css) : error()

struct FullRectSubmesh <: RectSubmesh end
FuReSu = FullRectSubmesh

valid_submesh(rm::ReMe, frs::FuReSu) = true

get_i_r(rm::ReMe, frs::FuReSu) = get_I(rm)
get_i_l(rm::ReMe, frs::FuReSu) = 1
get_j_u(rm::ReMe, frs::FuReSu) = get_J(rm)
get_j_d(rm::ReMe, frs::FuReSu) = 1

get_h_f_bs(rm::ReMe, frs::FuReSu) = get_h_f_bs(rm)

struct ContRectSubmesh <: RectSubmesh
    i_r::Int
    i_l::Int
    j_u::Int
    j_d::Int
    ContRectSubmesh(i_r, i_l, j_u, j_d) = begin
        valid = ((i_l > 0)    && (j_d > 0)    &&
                 (i_r >= i_l) && (j_u >= j_d)   )
        if ! valid
            error()
        end

        return new(i_r, i_l, j_u, j_d)
    end
end
CoReSu = ContRectSubmesh

get_i_r(crs::CoReSu) = crs.i_r
get_i_l(crs::CoReSu) = crs.i_l
get_j_u(crs::CoReSu) = crs.j_u
get_j_d(crs::CoReSu) = crs.j_d

valid_submesh(rm::ReMe, crs::CoReSu) = begin
    I, J = get_I(rm), get_J(rm)
    i_r, i_l = get_i_r(crs), get_i_l(crs)
    j_u, j_d = get_j_u(crs), get_j_d(crs)
    # TODO : might be able to reorder to short-circuit earlier
    # TODO : might be more effic. to calc. invalid
    valid = ((i_r <= I) && (i_l <= I) &&
             (j_u <= J) && (j_d <= J)   )
    return valid
end

# TODO : use of valid_submesh may be ineff.
get_i_r(rm::ReMe, crs::CoReSu) = valid_submesh(rm, crs) ? get_i_r(crs) : error()
get_i_l(rm::ReMe, crs::CoReSu) = valid_submesh(rm, crs) ? get_i_l(crs) : error()
get_j_u(rm::ReMe, crs::CoReSu) = valid_submesh(rm, crs) ? get_j_u(crs) : error()
get_j_d(rm::ReMe, crs::CoReSu) = valid_submesh(rm, crs) ? get_j_d(crs) : error()

# NOTE : Generate Enclosing Angle Collections

# TEMP : potentially able to remove
# gen_enc_HeCo(pq::PoQu, hc::HeCo) = hc

# NOTE : Sweep Orderings

gen_sweep_order(pq::PoQu, sm::SlMe, ac::AnCo, ss::SlSu) = begin
    i_r = get_i_r(sm, ss)
    i_l = get_i_l(sm, ss)

    so_is = is_r(ac) ? (i_l:1:i_r) : (is_l(ac) ? (i_r:-1:i_l) : error())

    so_hs = Arr{Arr{Int,1},1}(undef, length(so_is))
    r = 1
    for i in so_is
        so_hs[r] = [get_li(sm, i)]
        r += 1
    end

    return so_hs
end

# TODO : Old, should eventually remove
# gen_sweep_order(fq::FuQu, rm::ReMe, ac::AnCo, rs::ReSu) = begin
#     i_r = get_i_r(rm, rs)
#     i_l = get_i_l(rm, rs)
#     j_u = get_j_u(rm, rs)
#     j_d = get_j_d(rm, rs)

#     so_is = is_r(ac) ? (i_l:1:i_r) : (is_l(ac) ? (i_r:-1:i_l) : error())
#     so_js = is_u(ac) ? (j_d:1:j_u) : (is_d(ac) ? (j_u:-1:j_d) : error())

#     so_hs = Arr{Arr{Int,1},1}(undef, length(so_is)*length(so_js))
#     r = 1
#     for j in so_js
#         for i in so_is
#             so_hs[r] = [get_li(rm, i, j)]
#             r += 1
#         end
#     end

#     return so_hs
# end

# TODO : get working for arbitrary submeshes
gen_sweep_order(fq::FuQu, rm::ReMe, ac::AnCo, frs::FuReSu) = begin
    I = get_I(rm)
    J = get_J(rm)

    if     is_ru(ac)
        # start. corner indices
        ci, cj = 1, 1
        di, dj = +1, +1
    elseif is_lu(ac)
        ci, cj = I, 1
        di, dj = -1, +1
    elseif is_rd(ac)
        ci, cj = 1, J
        di, dj = +1, -1
    elseif is_ld(ac)
        ci, cj = I, J
        di, dj = -1, -1
    else
        error()
    end

    is = [ci]
    js = [cj]

    nd = I+J-1
    so_hs = Vec{Vec{Int}}(undef, nd)
    for d in 1:nd
        so_hs[d] = [get_li(rm, i, j) for (i, j) in zip(is, js)]

        is = [is; is[end] + di]
        js = [js .+ dj; js[end]]

        vs = @. (1 <= is <= I) & (1 <= js <= J)

        is = is[vs]
        js = js[vs]
    end

    return so_hs
end

# NOTE : Materials

abstract type Material end
Ma = Material

abstract type TranMaterial <: Material end
TrMa = TranMaterial
abstract type DiffMaterial <: Material end
DiMa = DiffMaterial

struct NeutTranMaterial <: TranMaterial
    Σ_ts :: Arr{Fl,2} # (G, H)
    Σ_ss :: Arr{Fl,3} # (G, G, H) (g', g, h)
    Σ_fs :: Arr{Fl,2} # (G, H)

    νs   :: Arr{Fl,2} # (G, H)
    χs   :: Arr{Fl,2} # (G, H)
    Qs   :: Arr{Fl,2} # (G, H)

    G    :: Int
    H    :: Int

    NeutTranMaterial(Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H) = begin
        valid = ((G > 0) && (H > 0)     &&
                 ((G,H) == size(Σ_ts)
                        == size(Σ_fs)
                        == size(νs  )
                        == size(χs  )
                        == size(Qs  ))  &&
                 ((G,G,H) == size(Σ_ss))  )
        if ! valid
            error()
        end

        return new(Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H)
    end
end
NeTrMa = NeutTranMaterial

function NeutTranMaterial(Σ_ts::Vec, Σ_ss::Mat, Σ_fs::Vec,
                          νs::Vec, χs::Vec, Qs::Vec, G, H)
    return NeutTranMaterial(repeat(Σ_ts, outer=(1, H   )),
                            repeat(Σ_ss, outer=(1, 1, H)),
                            repeat(Σ_fs, outer=(1, H   )),
                            repeat(νs  , outer=(1, H   )),
                            repeat(χs  , outer=(1, H   )),
                            repeat(Qs  , outer=(1, H   )),
                            G, H)
end

get_G(ntm::NeTrMa) = ntm.G
get_H(ntm::NeTrMa) = ntm.H

get_gs(ntm::NeTrMa) = 1:get_G(ntm)
get_hs(ntm::NeTrMa) = 1:get_H(ntm)

get_Σ_ts(ntm::NeTrMa) = ntm.Σ_ts
get_Σ_ss(ntm::NeTrMa) = ntm.Σ_ss
get_Σ_fs(ntm::NeTrMa) = ntm.Σ_fs
get_νs(ntm::NeTrMa)   = ntm.νs
get_χs(ntm::NeTrMa)   = ntm.χs
get_Qs(ntm::NeTrMa)   = ntm.Qs

get_Σ_ts(ntm::NeTrMa, gs , hs    ) = ntm.Σ_ts[gs , hs    ]
get_Σ_ss(ntm::NeTrMa, gps, gs, hs) = ntm.Σ_ss[gps, gs, hs]
get_Σ_fs(ntm::NeTrMa, gs , hs    ) = ntm.Σ_fs[gs , hs    ]
get_νs(  ntm::NeTrMa, gs , hs    ) = ntm.νs[  gs , hs    ]
get_χs(  ntm::NeTrMa, gs , hs    ) = ntm.χs[  gs , hs    ]
get_Qs(  ntm::NeTrMa, gs , hs    ) = ntm.Qs[  gs , hs    ]

struct PhotTranMaterial <: TranMaterial end
PhTrMa = PhotTranMaterial

# TODO : for the moment, we wrap $\hat{D}$, $D^{+}$, $D^{-}$ quantities in the
# MSED material structure. Kinda ugly tying together material and solver types.
# NOTE : H_f (no. total faces) (some face quantities, i.e. boundary faces may go
# unset / unused)
struct MSEDMaterial <: DiffMaterial
    D_ps :: Arr{Fl, 2} # (G, H_f) $D^{+}$
    D_ms :: Arr{Fl, 2} # (G, H_f) $D^{-}$
    D_hs :: Arr{Fl, 2} # (G, H_f) $\hat{D}$

    Σ_ts :: Arr{Fl, 2} # (G, H)
    Σ_ss :: Arr{Fl, 3} # (G, G, H) (g', g, h)
    Σ_fs :: Arr{Fl, 2} # (G, H)

    νs   :: Arr{Fl, 2} # (G, H)
    χs   :: Arr{Fl, 2} # (G, H)
    Qs   :: Arr{Fl, 2} # (G, H)

    G    :: Int
    H    :: Int
    H_f  :: Int

    MSEDMaterial(D_ps, D_ms, D_hs, Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H, H_f) = begin
        valid = ((G > 0) && (H > 0) && (H_f > 0) &&
                 ((G, H_f) == size(D_ps)
                           == size(D_ms)
                           == size(D_hs))        &&
                 ((G, H) == size(Σ_ts)
                         == size(Σ_fs)
                         == size(νs )
                         == size(χs )
                         == size(Qs ))           &&
                 ((G, G, H) == size(Σ_ss))         )
        if ! valid
            error()
        end

        return new(D_ps, D_ms, D_hs, Σ_ts, Σ_ss, Σ_fs, νs, χs, Qs, G, H, H_f)
    end
end
MMa = MSEDMaterial

function MSEDMaterial(D_ps::Vec, D_ms::Vec, D_hs::Vec,
                      Σ_ts::Vec, Σ_ss::Mat, Σ_fs::Vec,
                      νs::Vec, χs::Vec, Qs::Vec,
                      G, H, H_f)
    return MSEDMaterial(repeat(D_ps, outer=(1, H_f   )),
                        repeat(D_ms, outer=(1, H_f   )),
                        repeat(D_hs, outer=(1, H_f   )),
                        repeat(Σ_ts, outer=(1, H     )),
                        repeat(Σ_ss, outer=(1, 1  , H)),
                        repeat(Σ_fs, outer=(1, H     )),
                        repeat(νs  , outer=(1, H     )),
                        repeat(χs  , outer=(1, H     )),
                        repeat(Qs  , outer=(1, H     )),
                        G, H, H_f)
end

get_G(mm::MMa)   = mm.G
get_H(mm::MMa)   = mm.H
get_H_f(mm::MMa) = mm.H_f

get_gs(mm::MMa)   = 1:get_G(mm)
get_hs(mm::MMa)   = 1:get_H(mm)
get_h_fs(mm::MMa) = 1:get_H_f(mm)

get_D_ps(mm::MMa) = mm.D_ps
get_D_ms(mm::MMa) = mm.D_ms
get_D_hs(mm::MMa) = mm.D_hs
get_Σ_ts(mm::MMa) = mm.Σ_ts
get_Σ_ss(mm::MMa) = mm.Σ_ss
get_Σ_fs(mm::MMa) = mm.Σ_fs
get_νs(  mm::MMa) = mm.νs
get_χs(  mm::MMa) = mm.χs
get_Qs(  mm::MMa) = mm.Qs

get_D_ps(mm::MMa, gs , h_fs    ) = mm.D_ps[gs , h_fs    ]
get_D_ms(mm::MMa, gs , h_fs    ) = mm.D_ms[gs , h_fs    ]
get_D_hs(mm::MMa, gs , h_fs    ) = mm.D_hs[gs , h_fs    ]
get_Σ_ts(mm::MMa, gs , hs      ) = mm.Σ_ts[gs , hs      ]
get_Σ_ss(mm::MMa, gps, gs  , hs) = mm.Σ_ss[gps, gs  , hs]
get_Σ_fs(mm::MMa, gs , hs      ) = mm.Σ_fs[gs , hs      ]
get_νs(  mm::MMa, gs , hs      ) = mm.νs[  gs , hs      ]
get_χs(  mm::MMa, gs , hs      ) = mm.χs[  gs , hs      ]
get_Qs(  mm::MMa, gs , hs      ) = mm.Qs[  gs , hs      ]

get_Σ_f(mm::MMa, g::Int, h::Int) = mm.Σ_fs[g, h]


# NOTE : Spatial Discretization

abstract type SpatDisc end
SpDi = SpatDisc

abstract type TranDisc <: SpatDisc end
TrDi = TranDisc
abstract type DiffDisc <: SpatDisc end
DiDi = DiffDisc

abstract type FinDiff <: TranDisc end
FiDi = FinDiff

struct SlabFinDiff <: FinDiff
    αf_rl :: Fun
end
SlFiDi = SlabFinDiff

struct RectFinDiff <: FinDiff
    αf_rl :: Fun
    αf_ud :: Fun
end
ReFiDi = RectFinDiff

struct CubeFinDiff <: FinDiff end
CuFiDi = CubeFinDiff

# discretized on faces
gen_init_ψds(aq::AnQu, tm::TrMa, me::Me, fd::FiDi) = begin
    N   = get_N(aq)
    G   = get_G(tm)
    H_f = get_H_f(me)
    ψds = zeros(N, G, H_f)
    return ψds
end

# NOTE : generate representative cell-average scalar flux values
# TODO : make this take in ac's or angle indices :)
gen_ψhs(aq::AnQu, tm::TrMa, cm::CaMe, fd::FiDi, ac::AnCo, gs, hs, ψds::Arr{Fl,3}) = begin
    ns = get_ns(aq, ac)

    rμs  = get_μs(aq, ns)
    rdxs = get_dxs(cm, hs)

    αs_rl = fd.αf_rl(rμs, tm, rdxs, ns, gs, hs)

    h_f_rs = get_h_f_rs(cm, hs)
    h_f_ls = get_h_f_ls(cm, hs)

    ψhs = @. 0.5 * ((1 + αs_rl)*ψds[ns, gs, h_f_rs] + (1 - αs_rl)*ψds[ns, gs, h_f_ls])

    return ψhs
end

# pure, one-dimensional αf definitions
# pμs (angular cosines) (ROTATED COORD.)
# pdx (spatial length)  (ROTATED COORD.)
# NOTE : pdxs should be consistent w/ hs!!
# NOTE : pμs should be consistent w/ ns
αf_dd(pμs, tm::TrMa, pdxs, ns, gs, hs) = begin
    pN = length(ns)
    pG = length(gs)
    pH = length(hs)

    αs = zeros(pN, pG, pH)
    return αs
end
αf_su(pμs, tm::TrMa, pdxs, ns, gs, hs) = begin
    pN = length(ns)
    pG = length(gs)
    pH = length(hs)

    ss = sign.(pμs)

    αs = repeat(ss, outer=(1,pG,pH))
    return αs
end
αf_sc(pμs, ntm::NeTrMa, pdxs, ns, gs, hs) = begin
    pN = length(ns)
    pG = length(gs)
    pH = length(hs)

    pΣ_ts = get_Σ_ts(ntm, gs, hs)

    pμs   = reshape(pμs  , pN, 1 , 1 )
    pΣ_ts = reshape(pΣ_ts, 1 , pG, pH)
    pdxs  = reshape(pdxs , 1 , 1 , pH)

    sτs = @. 0.5 * pΣ_ts * pdxs / pμs
    αs  = @. coth(sτs) - 1. / sτs
    return αs
end

abstract type Bound end
Bo = Bound

abstract type TranBound <: Bound end
TrBo = TranBound

struct LazyTranBound <: TranBound end
LaTrBo = LazyTranBound

struct VacuTranBound <: TranBound end
VaTrBo = VacuTranBound

struct IsotTranBound <: TranBound
    ψi :: Fl
end
IsTrBo = IsotTranBound
get_ψi(itb::IsTrBo) = itb.ψi

struct ReflTranBound <: TranBound end
ReTrBo = ReflTranBound

struct PeriTranBound <: TranBound end
PeTrBo = PeriTranBound

struct TranBoundColl
    h_fs_tbs # TODO : type
    tbs      # TODO : type
end
TrBoCo = TranBoundColl
get_h_fs_tbs(tbc::TrBoCo) = tbc.h_fs_tbs
get_tbs(tbc::TrBoCo)      = tbc.tbs

function bound_setup!(aq::AnQu, tm::TrMa, me::Me, td::TrDi,
                      ac::AnCo, gs, h_fs, ltb::LaTrBo, ψds::Arr{Fl,3})
    # NOTE : LazyTB does nothing
    return nothing
end

function bound_setup!(aq::AnQu, tm::TrMa, me::Me, fd::FiDi,
                      ac::AnCo, gs, h_fs, vtb::VaTrBo, ψds::Arr{Fl,3})
    ns = get_ns(aq, ac)
    ψds[ns, gs, h_fs] .= 0.
end

function bound_setup!(aq::AnQu, tm::TrMa, me::Me, fd::FiDi,
                      ac::AnCo, gs, h_fs, itb::IsTrBo, ψds::Arr{Fl,3})
    ns = get_ns(aq, ac)
    ψi = get_ψi(itb)
    ψds[ns, gs, h_fs] .= ψi
end

# NOTE : must implement for each mesh, quadrature, angular collec.
get_ac_re(pq::PoQu, sm::SlMe, hc::HeCo, h_f::Int) = begin
    if     is_r(hc)
        HC_L
    elseif is_l(hc)
        HC_R
    else
        error()
    end
end
# TODO : ugly! this would be easier w/ indiv. direction vectors and surface
# normals!
get_ac_re(fq::FuQu, rm::ReMe, qc::QuCo, h_f::Int) = begin
    if     is_h_f_rl(rm, h_f)
        if     is_ru(qc)
            qc_re = QC_LU
        elseif is_lu(qc)
            qc_re = QC_RU
        elseif is_rd(qc)
            qc_re = QC_LD
        elseif is_ld(qc)
            qc_re = QC_RD
        else
            error()
        end
    elseif is_h_f_ud(rm, h_f)
        if     is_ru(qc)
            qc_re = QC_RD
        elseif is_lu(qc)
            qc_re = QC_LD
        elseif is_rd(qc)
            qc_re = QC_RU
        elseif is_ld(qc)
            qc_re = QC_LU
        else
            error()
        end
    else
        error()
    end

    return qc_re
end
function bound_setup!(aq::AnQu, tm::TrMa, me::Me, fd::FiDi,
                      ac::AnCo, gs, h_fs, rtb::ReTrBo, ψds::Arr{Fl,3})
    ns_in = get_ns(aq, ac)
    for h_f in h_fs
        ac_re = get_ac_re(aq, me, ac, h_f)
        ns_ou = get_ns(aq, ac_re)
        # NOTE : assumes ns_in and ns_ou are corresp. ordered!
        ψds[ns_in, gs, h_f] = ψds[ns_ou, gs, h_f]
    end
end

# NOTE : must implement for each mesh
get_h_f_pe(sm::SlMe, h_f::Int) = begin
    (h_f == 1) ? get_H_f(sm) : ((h_f == get_H_f(sm)) ? 1 : error())
end
function bound_setup!(aq::AnQu, tm::TrMa, me::Me, fd::FiDi,
                      ac::AnCo, gs, h_fs, ptb::PeTrBo, ψds::Arr{Fl,3})
    ns = get_ns(aq, ac)
    for h_f in h_fs
        h_f_pe = get_h_f_pe(me, hf)
        ψds[ns, gs, h_f] = ψds[ns, gs, h_f_pe]
    end
end

# NOTE : Default AngColl's, Energy Groups, SubMesh's

def_acs(sm::SlMe, pq::PoQu) = STD_HCS
def_acs(rm::ReMe, fq::FuQu) = STD_QCS
def_acs(cm::CuMe, fq::FuQu) = STD_OCS

def_gs(tm::TrMa) = 1:get_G(tm)

def_sm(sm::SlMe) = FuSlSu()
def_sm(rm::ReMe) = FuReSu()

# NOTE : Problem Definitions

abstract type Prob end
Pr = Prob

struct TranProb <: Prob
    aq :: AnQu  # angular quadrature
    tm :: TrMa  # materials
    me :: Me    # spatial mesh
    sd :: SpDi  # spatial discretization

    N   :: Int  # no. angles
    G   :: Int  # no. groups
    H   :: Int  # no. cells

    TranProb(aq, tm, me, sd, N, G, H) = begin
        valid = ((N == get_N(aq))              &&
                 (G == get_G(tm))              &&
                 (H == get_H(tm) == get_H(me))   )
        if ! valid
            error()
        end

        new(aq, tm, me, sd, N, G, H)
    end
end
TrPr = TranProb

# zero'th moment scaling factors
gen_iso_scal(sm::SlMe) = (1/2)
gen_iso_scal(rm::ReMe) = (1/(4*π))
gen_iso_scal(cm::CuMe) = (1/(4*π))

function cell_march!(pq::PoQu, ntm::NeTrMa, sm::SlMe, sfd::SlFiDi,
                     hc::HeCo, gs, hs, Ss::Arr{Fl,2}, ψds::Arr{Fl,3})
    ns = get_ns(pq, hc)

    h_f_in_rls = get_h_f_in_rls(pq, sm, hc, hs)
    h_f_ou_rls = get_h_f_ou_rls(pq, sm, hc, hs)

    # r → requested
    rN = length(ns)
    rG = length(gs)
    rH = length(hs)

    rμs  = get_μs(pq, ns)
    rΣ_ts = get_Σ_ts(ntm, gs, hs)
    rdxs = get_dxs(sm, hs)
    rSs  = Ss[gs, hs]
    rαs  = sfd.αf_rl(rμs, ntm, rdxs, ns, gs, hs)
    rψ_in_rls = ψds[ns, gs, h_f_in_rls]

    rμs   = reshape(rμs  , rN,  1,  1)
    rΣ_ts = reshape(rΣ_ts,  1, rG, rH)
    rdxs  = reshape(rdxs ,  1,  1, rH)
    rSs   = reshape(rSs  ,  1, rG, rH)
    # NOTE : r_αs & r_iψs should be correctly shaped
    if ! ((rN, rG, rH) == size(rαs) == size(rψ_in_rls))
        error()
    end

    # right-left sign
    s_rls = sign.(rμs)

    # TODO : add additional source angular moments (for now, only isotropic)
    is = gen_iso_scal(sm)

    rψ_ou_rls = @. begin
        (
         ( ( s_rls*rμs - 0.5*rΣ_ts*rdxs*( 1 - s_rls*rαs ) )*rψ_in_rls + is*rSs*rdxs )
         /
           ( s_rls*rμs + 0.5*rΣ_ts*rdxs*( 1 + s_rls*rαs ) )
        )
    end

    ψds[ns, gs, h_f_ou_rls] = rψ_ou_rls
end

function cell_march!(fq::FuQu, ntm::NeTrMa, rm::ReMe, rfd::ReFiDi,
                     qoc::QOCo, gs, hs, Ss::Arr{Fl,2}, ψds::Arr{Fl,3})
    ns = get_ns(fq, qoc)

    h_f_in_rls = get_h_f_in_rls(fq, rm, qoc, hs)
    h_f_ou_rls = get_h_f_ou_rls(fq, rm, qoc, hs)

    h_f_in_uds = get_h_f_in_uds(fq, rm, qoc, hs)
    h_f_ou_uds = get_h_f_ou_uds(fq, rm, qoc, hs)

    rN = length(ns)
    rG = length(gs)
    rH = length(hs)

    rμs = get_μs(fq, ns)
    rηs = get_ηs(fq, ns)

    rΣ_ts = get_Σ_ts(ntm, gs, hs)

    rdxs = get_dxs(rm, hs)
    rdys = get_dys(rm, hs)

    rSs = Ss[gs, hs]

    rα_μs = rfd.αf_rl(rμs, ntm, rdxs, ns, gs, hs)
    rα_ηs = rfd.αf_ud(rηs, ntm, rdys, ns, gs, hs)

    rψ_in_rls = ψds[ns, gs, h_f_in_rls]
    rψ_in_uds = ψds[ns, gs, h_f_in_uds]

    rμs   = reshape(rμs  , rN,  1,  1)
    rηs   = reshape(rηs  , rN,  1,  1)
    rΣ_ts = reshape(rΣ_ts,  1, rG, rH)
    rdxs  = reshape(rdxs ,  1,  1, rH)
    rdys  = reshape(rdys ,  1,  1, rH)
    rSs   = reshape(rSs  ,  1, rG, rH)
    if ! ((rN, rG, rH) == size(rα_μs)
                       == size(rα_ηs)
                       == size(rψ_in_rls)
                       == size(rψ_in_uds))
        error()
    end

    s_rls = sign.(rμs)
    s_uds = sign.(rηs)

    # TODO : add additional source angular moments (for now, only isotropic)
    is = gen_iso_scal(rm)

    # ψ_{i,j}
    # TODO : make this long expression legible (tried but failed :( )
    ψhs = @. begin
        ( ( ( ( ( s_rls*rμs )/( rdxs ) ) * ( ( ( 1 - s_rls*rα_μs ) / ( 1 +
        s_rls*rα_μs ) ) + 1 ) )*rψ_in_rls + ( ( ( s_uds*rηs )/( rdys ) ) * ( ( (
        1 - s_uds*rα_ηs ) / ( 1 + s_uds*rα_ηs ) ) + 1 ) )*rψ_in_uds + is*rSs ) / (
        rΣ_ts + 2*( ( s_rls*rμs ) / ( rdxs*( 1 + s_rls*rα_μs ) ) + ( s_uds*rηs )
        / ( rdys*( 1 + s_uds*rα_ηs ) ) ) ) )
    end

    rψ_ou_rls = @. ( 2*ψhs - ( 1 - s_rls*rα_μs )*rψ_in_rls ) / ( 1 + s_rls*rα_μs )
    rψ_ou_uds = @. ( 2*ψhs - ( 1 - s_uds*rα_ηs )*rψ_in_uds ) / ( 1 + s_uds*rα_ηs )

    ψds[ns, gs, h_f_ou_rls] = rψ_ou_rls
    ψds[ns, gs, h_f_ou_uds] = rψ_ou_uds
end

# NOTE : MUTATION OF ψds!!
function setup!(tp::TrPr, tbc::TrBoCo, ψds::Arr{Fl,3},
                acs = def_acs(tp.me, tp.aq),
                gs  = def_gs(tp.tm),
                sm  = def_sm(tp.me))
    # bound faces on submesh
    h_f_b_sms = get_h_f_bs(tp.me, sm)

    for (h_fs_tb, tb) in zip(tbc.h_fs_tbs, tbc.tbs)
        for ac in acs
            # incoming bound faces for AngColl
            h_f_ibs = get_h_f_ibs(tp.aq, tp.me, ac)
            h_fs = intersect(h_f_b_sms, h_fs_tb, h_f_ibs)
            bound_setup!(tp.aq, tp.tm, tp.me, tp.sd, ac, gs, h_fs, tb, ψds)
        end
    end
end

# NOTE : MUTATION OF ψds!!
# TODO : consider removing tp::TrPr, seems unnec.
function march!(tp::TrPr, Ss::Arr{Fl,2}, ψds::Arr{Fl,3},
                acs = def_acs(tp.me, tp.aq),
                gs  = def_gs(tp.tm),
                sm  = def_sm(tp.me))
    Threads.@threads for ac in acs
        for hs in gen_sweep_order(tp.aq, tp.me, ac, sm)
            cell_march!(tp.aq, tp.tm, tp.me, tp.sd, ac, gs, hs, Ss, ψds)
        end
    end
end

# NOTE : Diffusion

abstract type DiffBound <: Bound end
DiBo = DiffBound

# NOTE : Transport Corrected (TC)
struct ReflDiffBound <: DiffBound end
ReDiBo = ReflDiffBound

struct InTCDiffBound <: DiffBound
    bs    # (G, H_f) $B$, tcp marsh. corr. factor # TODO : type
    j_ins # (G, H_f) $J_{inc.}$, incoming current # TODO : type
end
InTCDiBo = InTCDiffBound

# TODO : transport corrected diffusion boundary
# struct PeTCDiffBound <: DiffBound
#     D_hs :: Arr{Fl, 2} # (G, H_f) $\hat{D}$
# end
# PeTCDiBo = PeTCDiffBound

struct DiffBoundColl
    h_fs_dbs # TODO : type
    dbs      # TODO : type
end
DiBoCo = DiffBoundColl

# NOTE : MSED Energy Collapse

# TODO : consider splitting into separate sub-functions :o
# TODO : generalize into arbitrary cart meshes
# gmm   : original MSEDMaterial (G groups          , g = 1, 2, ..., G)
# cmm   : collaped MSEDMaterial (C collapsed groups, c = 1, 2, ..., C)
# gϕs   : scalar flux weighting function (G, H)
# cgs   : group indices (g) for each collapsed group (c)
# e.g. cgs = [[1,2], [3,4]] would define a 2 group collapsed MSEDMaterial with
# c = 1 corresp. to g = 1, 2 and c = 2 corresp. to g = 3, 4 (from orig.
# MSEDMaterial)
function collapse_MSED(gmm::MMa, cm::CaMe, gϕs::Arr{Fl,2}, cgs)
    C   = length(cgs)
    G   = get_G(gmm)
    H   = get_H(cm)
    H_f = get_H_f(cm)

    cD_ps = zeros(C, H_f)
    cD_ms = zeros(C, H_f)
    cD_hs = zeros(C, H_f)

    cΣ_ts = zeros(C, H)
    cΣ_ss = zeros(C, C, H) # (c', c, h)
    cΣ_fs = zeros(C, H)

    cνs  = zeros(C, H)
    cχs  = zeros(C, H)
    cQs  = zeros(C, H)

    # cell center based quantities
    for h in get_hs(cm)
        for (c, gs) in enumerate(cgs)
            cΣ_ts[c, h] = (
                sum(get_Σ_ts(gmm, gs, h) .* gϕs[gs, h])
              /
                sum(gϕs[gs, h])
            )

            cχs[c, h] = sum(get_χs(gmm, gs, h))
            cQs[c, h] = sum(get_Qs(gmm, gs, h))

            for (cp, gps) in enumerate(cgs)
                cΣ_ss[cp, c, h] = (
                    sum([get_Σ_ss(gmm, gp, g, h)*gϕs[gp, h] for gp in gps, g in gs])
                  /
                    sum(gϕs[gps, h])
                )
            end

            # NOTE: MSED collapse combines ν & Σ_{f}, we have to choose how to
            # separate them back out!
            cνΣ_f_c_h = (
                sum(get_νs(gmm, gs, h) .* get_Σ_fs(gmm, gs, h) .* gϕs[gs, h])
              /
                sum(gϕs[gs, h])
            )
            # mean ν for the current collapsed group c
            cνm_c_h = mean(get_νs(gmm, gs, h))
            cνs[c, h] = cνm_c_h
            cΣ_fs[c, h] = cνΣ_f_c_h / cνm_c_h
        end
    end

    # cell edge based quantities (collapse need only be done on interior faces)
    for h_f in get_h_f_ints(cm)
        h_p = get_h_f_h_p(cm, h_f) # positive orient. cell index
        h_m = get_h_f_h_m(cm, h_f) # negative orient. cell index

        for (c, gs) in enumerate(cgs)
            cD_ps[c, h_f] = (
                sum(get_D_ps(gmm, gs, h_f) .* gϕs[gs, h_p])
              /
                sum(gϕs[gs, h_p])
            )
            cD_ms[c, h_f] = (
                sum(get_D_ms(gmm, gs, h_f) .* gϕs[gs, h_m])
              /
                sum(gϕs[gs, h_m])
            )
            cD_hs[c, h_f] = (
                sum(get_D_hs(gmm, gs, h_f) .* (gϕs[gs, h_p] + gϕs[gs, h_m]))
              /
                sum(gϕs[gs, h_p] + gϕs[gs, h_m])
            )
        end
    end

    cmm = MSEDMaterial(cD_ps, cD_ms, cD_hs,
                       cΣ_ts, cΣ_ss, cΣ_fs,
                       cνs, cχs, cQs,
                       C, H, H_f)

    return cmm
end
function collapse_DiBoCo(dbc::DiBoCo, cm::CaMe, gϕs::Arr{Fl,2}, cgs)
    C = length(cgs)
    G = size(gϕs, 1)
    H, H_f = get_H(cm), get_H_f(cm)

    c_dbs = []
    for (h_fs_db, db) in zip(dbc.h_fs_dbs, dbc.dbs)
        if     isa(db, ReflDiffBound)
            c_db = ReflDiffBound()
        elseif isa(db, InTCDiffBound)
            c_bs    = sa.spzeros(C, H_f)
            c_j_ins = sa.spzeros(C, H_f)
            # TODO : should this loop only go over h_fs for db of interest?
            for h_f_b in h_fs_db
                h_in = get_h_in(cm, h_f_b)
                for (c, gs) in enumerate(cgs)
                    c_bs[c, h_f_b] = (
                          sum(db.bs[gs, h_f_b] .* gϕs[gs, h_in])
                        /
                          sum(gϕs[gs, h_in])
                    )
                    c_j_ins[c, h_f_b] = sum(db.j_ins[gs, h_f_b])
                end
            end

            c_db = InTCDiffBound(c_bs, c_j_ins)
        else
            error()
        end

        push!(c_dbs, c_db)
    end

    c_dbc = DiBoCo(dbc.h_fs_dbs, c_dbs)

    return c_dbc
end
# NOTE : collapse scalar flux solutions
function collapse(gϕs::Arr{Fl,2}, cgs)
    G, H = size(gϕs)
    C = length(cgs)
    cϕs = zeros(C, H)
    for h in 1:H
        for (c, gs) in enumerate(cgs)
            cϕs[c, h] = sum(gϕs[gs, h])
        end
    end
    return cϕs
end
# NOTE : expand collapsed scalar flux solutions
# gϕs (previous higher-no.-group estimate)
function expand(gϕs::Arr{Fl,2}, cϕs::Arr{Fl,2}, cgs)
    # TODO : check cϕs has consist. size
    G, H = size(gϕs)
    C = length(cgs)

    # expanded estimate
    egϕs = zeros(G, H)
    for h in 1:H
        for (c, gs) in enumerate(cgs)
            egϕs[gs, h] = (gϕs[gs, h] / sum(gϕs[gs, h])) * cϕs[c, h]
        end
    end

    return egϕs
end

# Diff. Leakage + Collision Operator (Matrix)
# TODO : combine slab and rect. versions (very similar)
gen_mL(mm::MMa, slm::SlMe) = begin
    valid = ((get_H(mm)   == get_H(slm)  ) &&
             (get_H_f(mm) == get_H_f(slm))   )
    if ! valid
        error()
    end

    G   = get_G(mm)
    H   = get_H(slm)
    H_f = get_H_f(slm)

    I = get_I(slm)

    # NOTE : mL initialized w/ zeros, since below we actively mutate
    mL = sa.spzeros(G*H, G*H)

    # used to order matrix elements
    lis = LinearIndices((G, H))

    for h_c in get_hs(slm)
        # l, c, r   : left, center, right
        # f_l, f_r  : left-face, right-face
        # g         : group
        i_c, = get_ci(slm, h_c).I
        dx_c = get_dx(slm, h_c)

        for g in 1:G
            l_g_c = lis[g, h_c]

            Σ_t_g_c = get_Σ_ts(mm, g, h_c)

            mL[l_g_c, l_g_c] += Σ_t_g_c

            if i_c != I
                h_r   = get_h_r(slm, h_c)
                h_f_r = get_h_f_r(slm, h_c)

                l_g_r = lis[g, h_r]

                dx_r = get_dx(slm, h_r)
                dx_f_r = 0.5*(dx_c + dx_r)

                D_p_g_f_r = get_D_ps(mm, g, h_f_r)
                D_m_g_f_r = get_D_ms(mm, g, h_f_r)
                D_h_g_f_r = get_D_hs(mm, g, h_f_r)

                mL[l_g_c, l_g_c] += (1/dx_c)*(+ D_m_g_f_r/dx_f_r + D_h_g_f_r)
                mL[l_g_c, l_g_r] += (1/dx_c)*(- D_p_g_f_r/dx_f_r + D_h_g_f_r)
            end
            if i_c != 1
                h_l   = get_h_l(slm, h_c)
                h_f_l = get_h_f_l(slm, h_c)

                l_g_l = lis[g, h_l]

                dx_l = get_dx(slm, h_l)
                dx_f_l = 0.5*(dx_c + dx_l)

                D_p_g_f_l = get_D_ps(mm, g, h_f_l)
                D_m_g_f_l = get_D_ms(mm, g, h_f_l)
                D_h_g_f_l = get_D_hs(mm, g, h_f_l)

                mL[l_g_c, l_g_l] += (1/dx_c)*(- D_m_g_f_l/dx_f_l - D_h_g_f_l)
                mL[l_g_c, l_g_c] += (1/dx_c)*(+ D_p_g_f_l/dx_f_l - D_h_g_f_l)
            end
        end
    end

    return mL
end

function gen_mL(mm::MMa, rem::ReMe)
    valid = ((get_H(mm)   == get_H(rem)  ) &&
             (get_H_f(mm) == get_H_f(rem))   )
    if ! valid
        error()
    end

    G   = get_G(mm)
    H   = get_H(rem)
    H_f = get_H_f(rem)

    # no. boundary faces
    H_f_b = length(get_h_f_bs(rem))
    # no. elem. in sparse matrix construct.
    L::Int = G * (H + 8*H - 2*H_f_b)
    is = Vector{Int}(undef, L)
    js = Vector{Int}(undef, L)
    vs = Vector{Float64}(undef, L)

    I = get_I(rem)
    J = get_J(rem)

    lis = LinearIndices((G, H))

    l = 1
    for h_c in get_hs(rem)
        # l, r, u, d, c      : left, right, up, down, center
        # f_l, f_r, f_u, f_d : left-face, right-face, up-face, down-face
        # g                  : group
        i_c, j_c = get_ci(rem, h_c).I
        dx_c = get_dx(rem, h_c)
        dy_c = get_dy(rem, h_c)

        for g in 1:G
            l_g_c = lis[g, h_c]

            Σ_t_g_c = get_Σ_ts(mm, g, h_c)

            is[l] = l_g_c
            js[l] = l_g_c
            vs[l] = Σ_t_g_c
            l += 1

            if i_c != I
                h_r   = get_h_r(rem, h_c)
                h_f_r = get_h_f_r(rem, h_c)

                l_g_r = lis[g, h_r]

                dx_r  = get_dx(rem, h_r)
                dx_f_r = 0.5*(dx_c + dx_r)

                D_p_g_f_r = get_D_ps(mm, g, h_f_r)
                D_m_g_f_r = get_D_ms(mm, g, h_f_r)
                D_h_g_f_r = get_D_hs(mm, g, h_f_r)

                is[l] = l_g_c
                js[l] = l_g_c
                vs[l] = (1/dx_c)*(+ D_m_g_f_r/dx_f_r + D_h_g_f_r)
                l += 1

                is[l] = l_g_c
                js[l] = l_g_r
                vs[l] = (1/dx_c)*(- D_p_g_f_r/dx_f_r + D_h_g_f_r)
                l += 1
            end
            if i_c != 1
                h_l   = get_h_l(rem, h_c)
                h_f_l = get_h_f_l(rem, h_c)

                l_g_l = lis[g, h_l]

                dx_l = get_dx(rem, h_l)
                dx_f_l = 0.5*(dx_c + dx_l)

                D_p_g_f_l = get_D_ps(mm, g, h_f_l)
                D_m_g_f_l = get_D_ms(mm, g, h_f_l)
                D_h_g_f_l = get_D_hs(mm, g, h_f_l)

                is[l] = l_g_c
                js[l] = l_g_l
                vs[l] = (1/dx_c)*(- D_m_g_f_l/dx_f_l - D_h_g_f_l)
                l += 1

                is[l] = l_g_c
                js[l] = l_g_c
                vs[l] = (1/dx_c)*(+ D_p_g_f_l/dx_f_l - D_h_g_f_l)
                l += 1
            end
            if j_c != J
                h_u   = get_h_u(rem, h_c)
                h_f_u = get_h_f_u(rem, h_c)

                l_g_u = lis[g, h_u]

                dy_u = get_dy(rem, h_u)
                dy_f_u = 0.5*(dy_c + dy_u)

                D_p_g_f_u = get_D_ps(mm, g, h_f_u)
                D_m_g_f_u = get_D_ms(mm, g, h_f_u)
                D_h_g_f_u = get_D_hs(mm, g, h_f_u)

                is[l] = l_g_c
                js[l] = l_g_c
                vs[l] = (1/dy_c)*(+ D_m_g_f_u/dy_f_u + D_h_g_f_u)
                l += 1

                is[l] = l_g_c
                js[l] = l_g_u
                vs[l] = (1/dy_c)*(- D_p_g_f_u/dy_f_u + D_h_g_f_u)
                l += 1
            end
            if j_c != 1
                h_d   = get_h_d(rem, h_c)
                h_f_d = get_h_f_d(rem, h_c)

                l_g_d = lis[g, h_d]

                dy_d = get_dy(rem, h_d)
                dy_f_d = 0.5*(dy_c + dy_d)

                D_p_g_f_d = get_D_ps(mm, g, h_f_d)
                D_m_g_f_d = get_D_ms(mm, g, h_f_d)
                D_h_g_f_d = get_D_hs(mm, g, h_f_d)

                is[l] = l_g_c
                js[l] = l_g_d
                vs[l] = (1/dy_c)*(- D_m_g_f_d/dy_f_d - D_h_g_f_d)
                l += 1

                is[l] = l_g_c
                js[l] = l_g_c
                vs[l] = (1/dy_c)*(+ D_p_g_f_d/dy_f_d - D_h_g_f_d)
                l += 1
            end
        end
    end

    mL = sa.sparse(is, js, vs, G*H, G*H, +)

    return mL
end

# Scattering Operator (Matrix)
gen_mS(mm::MMa, cm::CaMe) = begin
    valid = (get_H(mm) == get_H(cm))
    if ! valid
        error()
    end

    G = get_G(mm)
    H = get_H(cm)

    L::Int = G*G*H # no. nonzero elems
    is = Vector{Int}(undef, L)
    js = Vector{Int}(undef, L)
    vs = Vector{Float64}(undef, L)

    # used to order matrix elements
    lis = LinearIndices((G, H))

    l = 1 # nonzero counter
    for h_c in 1:H
        for g in 1:G
            for gp in 1:G
                is[l] = lis[g, h_c]
                js[l] = lis[gp, h_c]
                vs[l] = get_Σ_ss(mm, gp, g, h_c)

                l += 1
            end
        end
    end

    mS = sa.sparse(is, js, vs, G*H, G*H)

    return mS
end
# Fission Operator (Matrix)
gen_mF(mm::MMa, cm::CaMe) = begin
    valid = (get_H(mm) == get_H(cm))
    if ! valid
        error()
    end

    G = get_G(mm)
    H = get_H(cm)

    L = G*G*H # no. nonzero elems
    is = Vector{Int}(undef, L)
    js = Vector{Int}(undef, L)
    vs = Vector{Float64}(undef, L)

    # used to order matrix elements
    lis = LinearIndices((G, H))

    l = 1 # nonzero counter
    for h_c in 1:H
        for g in 1:G
            for gp in 1:G
                is[l] = lis[g, h_c]
                js[l] = lis[gp, h_c]
                vs[l] = get_χs(mm, g, h_c) * get_νs(mm, gp, h_c) * get_Σ_fs(mm, gp, h_c)

                l += 1
            end
        end
    end

    mF = sa.sparse(is, js, vs, G*H, G*H)

    return mF
end
# Inhomogeneous Source Function (Vector)
gen_vQ(mm::MMa, cm::CaMe) = begin
    valid = ((get_H(mm)   == get_H(cm)  ) &&
             (get_H_f(mm) == get_H_f(cm))   )
    if ! valid
        error()
    end

    G = get_G(mm)
    H = get_H(cm)

    vQ = zeros(G*H)

    # used to order vector elements
    lis = LinearIndices((G, H))

    for h_c in 1:H
        for g in 1:G
            Q_g_c = get_Qs(mm, g, h_c)

            q_g_c = lis[g, h_c]
            vQ[q_g_c] += Q_g_c
        end
    end

    return vQ
end

# TODO : D.R.Y. up gen_mLb and gen_vQb (they look kinda similar)
# Boundary Diff. Leakage + Collision Operator (Matrix)
gen_mLb(mm::MMa, cm::CaMe, dbc::DiBoCo) = begin
    valid = ((get_H(mm)   == get_H(cm)  ) &&
             (get_H_f(mm) == get_H_f(cm))   )
    if ! valid
        error()
    end

    G = get_G(mm)
    H = get_H(cm)

    h_f_bs = get_h_f_bs(cm)

    mLb = sa.spzeros(G*H, G*H)

    # used to order matrix elements
    lis = LinearIndices((G, H))

    for (h_fs_db, db) in zip(dbc.h_fs_dbs, dbc.dbs)
        # only access valid mesh boundary faces
        h_fs = intersect(h_f_bs, h_fs_db)

        # TODO : sep. functions like bound_setup!?
        if     isa(db, ReDiBo)
            # do nothing
        elseif isa(db, InTCDiBo)
            for h_f in h_fs
                h_in  = get_h_in(cm, h_f)

                # NOTE : this ordering is important since at the moment SlMe and
                # ReMe have not implemented is_h_f_ud, is_hf_oi and is_hf_oi,
                # respectively
                # ds_in : inner cell charac. length along boundary face normal
                if     is_h_f_rl(cm, h_f)
                    ds_in = get_dx(cm, h_in)
                elseif is_h_f_ud(cm, h_f)
                    ds_in = get_dy(cm, h_in)
                elseif is_h_f_oi(cm, h_f)
                    ds_in = get_dz(cm, h_in)
                else
                    error()
                end

                for g in 1:G
                    l_g_h_in = lis[g, h_in]
                    mLb[l_g_h_in, l_g_h_in] += db.bs[g, h_f] / ds_in
                end
            end
        else
            error()
        end
    end

    return mLb
end
# Inhomogeneous Boundary Source Function (Vector)
gen_vQb(mm::MMa, cm::CaMe, dbc::DiBoCo) = begin
    valid = ((get_H(mm)   == get_H(cm)  ) &&
             (get_H_f(mm) == get_H_f(cm))   )
    if ! valid
        error()
    end

    G = get_G(mm)
    H = get_H(cm)

    h_f_bs = get_h_f_bs(cm)

    vQb = zeros(G*H)

    # used to order vector elements
    lis = LinearIndices((G, H))

    for (h_fs_db, db) in zip(dbc.h_fs_dbs, dbc.dbs)
        # only access valid mesh boundary faces
        h_fs = intersect(h_f_bs, h_fs_db)

        if     isa(db, ReDiBo)
            # do nothing
        elseif isa(db, InTCDiBo)
            for h_f in h_fs
                h_in  = get_h_in(cm, h_f)

                # same NOTE as above in gen_mLb
                if     is_h_f_rl(cm, h_f)
                    ds_in = get_dx(cm, h_in)
                elseif is_h_f_ud(cm, h_f)
                    ds_in = get_dy(cm, h_in)
                elseif is_h_f_oi(cm, h_f)
                    ds_in = get_dz(cm, h_in)
                else
                    error()
                end

                for g in 1:G
                    l_g_h_in = lis[g, h_in]
                    vQb[l_g_h_in] += 2 * db.j_ins[g, h_f] / ds_in
                end
            end
        else
            error()
        end

    end

    return vQb
end

# modified from Slab
gen_cf_rel(ϵ::Fl; δ::Fl = ϵ^2, l::Number = Inf) = begin
    return (o, n) -> begin
        c = la.norm((@. (n - o) / (abs(n) + δ)), l) < ϵ
        return c
    end
end

# never conv fun
ncf = (any...) -> false

λsf_ident = (λs_p::Fl, λ_p::Fl, λ_n::Fl) -> λs_p
gen_λsf_parcs(r::Fl) = begin
    λsf_parcs(λs_p::Fl, λ_p::Fl, λ_n::Fl) = begin
        return max(r*λ_n - 10*abs(λ_n - λ_p), 0.0)
    end
    return λsf_parcs
end

def_parcs_r = 0.90
def_λsf = gen_λsf_parcs(def_parcs_r)

gen_rel_cf_pi(ϵ::Fl; δ::Fl = ϵ^2, l::Number = Inf) = begin
    pcf = gen_cf_rel(ϵ; δ=δ, l=l)
    rel_cf(M, F, ϕs_p, ϕs_n, λ_p, λ_n) = begin
        c = pcf(ϕs_p, ϕs_n) && pcf(λ_p, λ_n)
        return c
    end
    return rel_cf
end
gen_res_cf_pi(ϵ::Fl; l::Number = Inf) = begin
    res_cf(M, F, ϕs_p, ϕs_n, λ_p, λ_n) = begin
        c = la.norm((M*ϕs_n - λ_n*F*ϕs_n), l) < ϵ
        return c
    end
    return res_cf
end

# power-outer
def_res_ϵpo = 1.e-10
def_cf_po = gen_res_cf_pi(def_res_ϵpo)

# power-inner (innermost iter. of MSED V-cycle)
def_res_ϵpi = 1.e-1 * def_res_ϵpo
def_cf_pi = gen_res_cf_pi(def_res_ϵpi)

# modified from Slab
# power iteration to solve M ϕ = λ F ϕ, (M & F square)
function power_iterate(M::AbMat{Fl}, F::AbMat{Fl};
                       ϕs_i  :: AbVec{Fl} = ones(size(M,1)),
                       λ_i   :: Fl        = 1.             ,
                       cf    :: Fun       = def_cf_po      ,
                       mi    :: Number    = Inf            ,
                       λsf   :: Fun       = def_λsf        ,
                       λs_i  :: Fl        = 0.             ,
                       save  :: Bool      = false           )
    if (mi < 0)
        error()
    end

    if save
        ϕss = []
        λss = []
    end

    ϕs_p = ϕs_i
    λ_p  = λ_i
    λs_p = λs_i # λ-shift!

    ni = 0

    (ni == mi) ? (@goto lab_maxi) : nothing
    while true
        Mt_p = M - λs_p*F
        ϕs_n  = Mt_p \ ((λ_p - λs_p) * F * ϕs_p)
        λ_n = (λ_p - λs_p) * (la.dot(ϕs_n, ϕs_p) / la.dot(ϕs_n, ϕs_n)) + λs_p
        λs_n = λsf(λs_p, λ_p, λ_n)

        ni += 1
        conv = cf(M, F, ϕs_p, ϕs_n, λ_p, λ_n)

        if save
            push!(ϕss, ϕs_n)
            push!(λss, λ_n)
        end

        ϕs_p = ϕs_n
        λ_p  = λ_n
        λs_p = λs_n

        (ni == mi) ? (@goto lab_maxi) : nothing
        conv ? (@goto lab_conv) : nothing
    end
    @label lab_maxi
    @label lab_conv

    return save ? (ϕss, λss, ni) : (ϕs_p, λ_p, ni)
end

# recursive MSED V-cycle func.
# TODO : consider making gϕs_i a vector (to match arguments of power_iterate
# fun.)
function rec_msed_power(gmm::MMa, cm::CaMe, gdbc::DiBoCo, gϕs_i::Arr{Fl,1}, gλ_i::Fl,
                        co_n_pr, co_n_po, co_cgs, co_λsf, co_λsif,
                        cf, mi)
    npr, npo, ncg = length(co_n_pr), length(co_n_po), length(co_cgs)
    nsf, nsi = length(co_λsf), length(co_λsif)

    valid = ((npr == npo == ncg)  &&
             (nsf == nsi)         &&
             (nsf == (npr + 1))     )
    if ! valid
        error()
    end

    G = get_G(gmm)
    H = get_H(cm)

    # construct g-group matrices
    gmL  = gen_mL(gmm, cm)
    gmLb = gen_mLb(gmm, cm, gdbc)
    gmS  = gen_mS(gmm, cm)
    gmF  = gen_mF(gmm, cm)
    gmM = (gmL + gmLb - gmS)

    # if we are at the bottom (top?, end?) of the call stack, just power iterate
    if (npr == 0)
        gϕs, gλ, ni = power_iterate(gmM, gmF;
                                    ϕs_i = gϕs_i    , λ_i  = gλ_i,
                                    cf   = cf       , mi   = mi,
                                    λsf  = co_λsf[1], λs_i = co_λsif[1](gλ_i),
                                    save = false)
        return gϕs, gλ
    end

    # pre- power-iterates
    gϕs_pr, gλ_pr = power_iterate(gmM, gmF;
                                  ϕs_i = gϕs_i    , λ_i  = gλ_i,
                                  cf   = ncf      , mi   = co_n_pr[1],
                                  λsf  = co_λsf[1], λs_i = co_λsif[1](gλ_i),
                                  save = false)

    # group collapse
    cmm    = collapse_MSED(gmm, cm, reshape(gϕs_pr, G, H), co_cgs[1])
    cdbc   = collapse_DiBoCo(gdbc, cm, reshape(gϕs_pr, G, H), co_cgs[1])
    C      = get_G(cmm)
    cϕs_pr = collapse(reshape(gϕs_pr, G, H), co_cgs[1])
    cλ_pr  = gλ_pr

    # NOTE : make sure this recursion stops eventually :)
    cϕs, cλ = rec_msed_power(cmm, cm, cdbc, reshape(cϕs_pr, C*H), cλ_pr,
                             co_n_pr[2:end], co_n_po[2:end], co_cgs[2:end],
                             co_λsf[2:end], co_λsif[2:end],
                             cf, mi)

    # group expansion
    exp_cϕs = expand(reshape(gϕs_pr, G, H), reshape(cϕs, C, H), co_cgs[1])

    # post power-iterates
    gϕs_po, gλ_po = power_iterate(gmM, gmF;
                                  ϕs_i = reshape(exp_cϕs, G*H), λ_i  = cλ,
                                  cf   = ncf                  , mi   = co_n_po[1],
                                  λsf  = co_λsf[1]            , λs_i = co_λsif[1](cλ),
                                  save = false)

    return gϕs_po, gλ_po
end

function msed_power_iterate(mm::MMa, cm::CaMe, dbc::DiBoCo,
                            co_n_pr, co_n_po, co_cgs, co_λsf, co_λsif;
                            ϕs_i :: AbVec{Fl} = ones(get_G(mm)*get_H(cm)),
                            λ_i  :: Fl        = 1.,
                            cfo  :: Fun       = def_cf_po,
                            mo   :: Number    = Inf,
                            cfi  :: Fun       = def_cf_pi,
                            mi   :: Number    = Inf,
                            save :: Bool      = false)
    # NOTE : need to pre-generate these for outer convergence residual check!
    mL  = gen_mL(mm, cm)
    mLb = gen_mLb(mm, cm, dbc)
    mS  = gen_mS(mm, cm)
    mF  = gen_mF(mm, cm)
    mM  = mL + mLb - mS
    # TODO : boundary conditions (mLb)

    if (mo < 0)
        error()
    end

    if save
        ϕss = []
        λss = []
    end

    ϕs_p = ϕs_i
    λ_p  = λ_i

    no = 0

    (no == mo) ? (@goto lab_maxo) : nothing
    while true
        ϕs_n, λ_n = rec_msed_power(mm, cm, dbc, ϕs_p, λ_p,
                                   co_n_pr, co_n_po, co_cgs, co_λsf, co_λsif,
                                   cfi, mi)

        no += 1
        co = cfo(mM, mF, ϕs_p, ϕs_n, λ_p, λ_n)

        if save
            push!(ϕss, ϕs_n)
            push!(λss, λ_n)
        end

        ϕs_p = ϕs_n
        λ_p  = λ_n

        (no == mo) ? (@goto lab_maxo) : nothing
        co         ? (@goto lab_cono) : nothing
    end
    @label lab_maxo
    @label lab_cono

    return save ? (ϕss, λss, no) : (ϕs_p, λ_p, no)
end

# NOTE : For the full, G group, case, D_ps, and D_ms are the same!
function gen_ds(cm::CaMe, Σ_ts::Arr{Fl,2})
    cm_H = get_H(cm)
    st_G, st_H = size(Σ_ts)
    valid = (cm_H == st_H)
    if ! valid
        error()
    end

    G   = st_G
    H   = cm_H
    H_f = get_H_f(cm)

    # NOTE : only interior faces set (boundaries left at zero)
    ds = zeros(G, H_f)

    for h_f in get_h_f_ints(cm)
        h_p = get_h_f_h_p(cm, h_f)
        h_m = get_h_f_h_m(cm, h_f)

        # TODO : implement get_ds function :o
        # NOTE : order matters (some is_... not implem. yet for some meshes
        if     is_h_f_rl(cm, h_f)
            ds_p = get_dx(cm, h_p)
            ds_m = get_dx(cm, h_m)
        elseif is_h_f_ud(cm, h_f)
            ds_p = get_dy(cm, h_p)
            ds_m = get_dy(cm, h_m)
        elseif is_h_f_oi(cm, h_f)
            ds_p = get_dz(cm, h_p)
            ds_m = get_dz(cm, h_m)
        else
            error()
        end

        for g in 1:G
            Σ_t_g_p = Σ_ts[g, h_p]
            Σ_t_g_m = Σ_ts[g, h_m]

            ds[g, h_f] = (1/3)*(ds_p + ds_m)/(ds_p*Σ_t_g_p + ds_m*Σ_t_g_m)
        end
    end

    return ds
end

# cm      : Cartesian Mesh
# ϕs      : cell scalar flux (G, H)
# jns     : J_{n} (n = i, j, k) (G, H_f)
# dcs     : cell-edge/face diffusion coefficients
function gen_dhs(cm::CaMe, ϕs::Arr{Fl,2}, jns::Arr{Fl,2}, dcs::Arr{Fl,2})
    cm_H, cm_H_f = get_H(cm), get_H_f(cm)
    ϕs_G, ϕs_H   = size(ϕs)
    jn_G, jn_H_f = size(jns)
    dc_G, dc_H_f = size(dcs)

    valid = ((cm_H   == ϕs_H)             &&
             (cm_H_f == jn_H_f == dc_H_f) &&
             (ϕs_G   == jn_G   == dc_G)     )
    if ! valid
        error()
    end

    G, H, H_f = ϕs_G, cm_H, cm_H_f

    # NOTE : only interior faces set (boundaries left at zero)
    dhs = zeros(G, H_f)

    for h_f in get_h_f_ints(cm)
        h_p = get_h_f_h_p(cm, h_f)
        h_m = get_h_f_h_m(cm, h_f)

        if     is_h_f_rl(cm, h_f)
            ds_p = get_dx(cm, h_p)
            ds_m = get_dx(cm, h_m)
        elseif is_h_f_ud(cm, h_f)
            ds_p = get_dy(cm, h_p)
            ds_m = get_dy(cm, h_m)
        elseif is_h_f_oi(cm, h_f)
            ds_p = get_dz(cm, h_p)
            ds_m = get_dz(cm, h_m)
        else
            error()
        end

        ds_h_f = 0.5*(ds_p + ds_m)

        for g in 1:G
            jn_g_h_f = jns[g, h_f]
            dc_g_h_f = dcs[g, h_f]
            ϕ_g_h_p  = ϕs[g, h_p]
            ϕ_g_h_m  = ϕs[g, h_m]

            dhs[g, h_f] = (
                  (jn_g_h_f + (dc_g_h_f / ds_h_f)*(ϕ_g_h_p - ϕ_g_h_m))
                /
                  (ϕ_g_h_p + ϕ_g_h_m)
            )
        end
    end

    return dhs
end

# TODO : ugly implementation!
# helper for arbit. ang. moments ls
function gen_mls(ls::Arr{Fl,1}, ws::Arr{Fl,1}, ψds::Arr{Fl})
    ls_N = length(ls)
    ws_N = length(ws)

    ψs_N, = size(ψds)

    valid = (ls_N == ws_N == ψs_N)
    if ! valid
        error()
    end

    mls = reshape(mapslices((ψs) -> sum(ls .* ψs .* ws), ψds, dims = 1), size(ψds)[2:end]...)

    return mls
end

# scalar flux
function gen_ϕs(aq::AnQu, ψds::Arr{Fl})
    N  = get_N(aq)
    ws = get_ws(aq)
    ϕs = gen_mls(ones(N), ws, ψds)
    return ϕs
end

# NOTE : PoQu only able to calculate i comp. of current
# current (i'th comp.)
function gen_jis(aq::AnQu, ψds::Arr{Fl})
    μs = get_μs(aq)
    ws = get_ws(aq)
    jis = gen_mls(μs, ws, ψds)
    return jis
end
# current (j'th comp.)
function gen_jjs(fq::FuQu, ψds::Arr{Fl})
    ηs = get_ηs(fq)
    ws = get_ws(fq)
    jjs = gen_mls(ηs, ws, ψds)
    return jjs
end
# current (k'th comp.)
function gen_jks(fq::FuQu, ψds::Arr{Fl})
    ξs = get_ξs(fq)
    ws = get_ws(fq)
    jks = gen_mls(ξs, ws, ψds)
    return jks
end

# Bn (i'th comp.) (transport corrected incident boundary factor NUMERATOR!!!)
function gen_bnis(aq::AnQu, ψds::Arr{Fl})
    μs = get_μs(aq)
    ws = get_ws(aq)
    bnis = gen_mls(abs.(μs), ws, ψds)
    return bnis
end
# Bn (j'th comp.)
function gen_bnjs(fq::FuQu, ψds::Arr{Fl})
    ηs = get_ηs(fq)
    ws = get_ws(fq)
    bnjs = gen_mls(abs.(ηs), ws, ψds)
    return bnjs
end
# Bn (k'th comp.)
function gen_bnks(fq::FuQu, ψds::Arr{Fl})
    ξs = get_ξs(fq)
    ws = get_ws(fq)
    bnks = gen_mls(abs.(ξs), ws, ψds)
    return bnks
end


function gen_jns(aq::AnQu, cm::CaMe, ψds::Arr{Fl,3})
    aq_N = get_N(aq)
    cm_H_f = get_H_f(cm)
    ψd_N, ψd_G, ψd_H_f = size(ψds)

    valid = ((aq_N   == ψd_N)   &&
             (cm_H_f == ψd_H_f)   )
    if ! valid
        error()
    end

    N, G, H_f = aq_N, ψd_G, cm_H_f

    jns = zeros(G, H_f)

    for h_f in get_h_f_ints(cm)
        ψd = ψds[:, :, h_f]

        if     is_h_f_rl(cm, h_f)
            jn = gen_jis(aq, ψd)
        elseif is_h_f_ud(cm, h_f)
            jn = gen_jjs(aq, ψd)
        elseif is_h_f_oi(cm, h_f)
            jn = gen_jks(aq, ψd)
        else
            error()
        end

        jns[:, h_f] = jn
    end

    return jns
end

# calculate boundary incoming currents
# TODO : Disgusting! Fix
function gen_jbis(aq::AnQu, cm::CaMe, ψds::Arr{Fl,3})
    aq_N = get_N(aq)
    cm_H_f = get_H_f(cm)
    ψd_N, ψd_G, ψd_H_f = size(ψds)

    valid = ((aq_N   == ψd_N)   &&
             (cm_H_f == ψd_H_f)   )
    if ! valid
        error()
    end

    N, G, H_f = aq_N, ψd_G, cm_H_f

    jbis = sa.spzeros(G, H_f)

    # r used to denote incoming anglular cosine
    if     isa(cm, SlMe)
        # NOTE : must be consistently ordered
        funs_ghfb = [get_h_f_rbs, get_h_f_lbs]
        funs_gr   = [get_μs     , get_μs     ]
        hcs_in    = [HC_L       , HC_R       ]
    elseif isa(cm, ReMe)
        funs_ghfb = [get_h_f_rbs, get_h_f_lbs, get_h_f_ubs, get_h_f_dbs]
        funs_gr   = [get_μs     , get_μs     , get_ηs     , get_ηs     ]
        hcs_in    = [HC_L       , HC_R       , HC_D       , HC_U       ]
    elseif isa(cm, CuMe)
        # TODO : implement
        error()
    else
        error()
    end

    for (ghfb, get_rs, hc_in) in zip(funs_ghfb, funs_gr, hcs_in)
        h_f_bs = ghfb(cm)
        ns_in  = get_ns(aq, hc_in)
        rs_in  = get_rs(aq, ns_in)
        ws_in  = get_ws(aq, ns_in)

        for h_f_b in h_f_bs
            for g in 1:G
                jbis[g, h_f_b] = sum(abs.(rs_in) .* ψds[ns_in, g, h_f_b] .* ws_in)
            end
        end
    end

    return jbis
end

# NOTE : assumes ψds edge based!
function gen_bs(aq::AnQu, tm::TrMa, cm::CaMe, fd::FiDi, ψds::Arr{Fl,3})
    aq_N               = get_N(aq)
    tm_G, tm_H         = get_G(tm), get_H(tm)
    cm_H, cm_H_f       = get_H(cm), get_H_f(cm)
    ψd_N, ψd_G, ψd_H_f = size(ψds)

    # TODO : these valid checks are getting ugly!
    valid = ((aq_N   == ψd_N)   &&
             (tm_G   == ψd_G)   &&
             (tm_H   == cm_H)   &&
             (cm_H_f == ψd_H_f)   )
    if ! valid
        error()
    end

    N, G, H, H_f = aq_N, tm_G, cm_H, cm_H_f

    # NOTE : sparse, only populated on boundaries
    bs = sa.spzeros(G, H_f)

    # TODO : slow! increase performance
    for h_f_b in get_h_f_bs(cm)
        ψd_b  = ψds[:,:,h_f_b]

        h_in  = get_h_in(cm, h_f_b)
        ψh_in = gen_ψhs(aq, tm, cm, fd, SC, 1:G, h_in, ψds)
        ϕh_in = gen_ϕs(aq, ψh_in)

        if     is_h_f_rl(cm, h_f_b)
            bn = gen_bnis(aq, ψd_b)
        elseif is_h_f_ud(cm, h_f_b)
            bn = gen_bnjs(aq, ψd_b)
        elseif is_h_f_oi(cm, h_f_b)
            bn = gen_bnks(aq, ψd_b)
        else
            error()
        end

        bs[:, h_f_b] = bn ./ ϕh_in
    end

    return bs
end

# generate a transport corrected MSED Material
function gen_MSEDMaterial(aq::AnQu, ntm::NeTrMa, cm::CaMe, fd::FiDi, ψds::Arr{Fl,3})
    # TODO : error checking
    N, G, H, H_f = get_N(aq), get_G(ntm), get_H(cm), get_H_f(cm)

    # cell-center angular fluxes
    ψhs = gen_ψhs(aq, ntm, cm, fd, SC, 1:G, 1:H, ψds)
    # cell-center scalar fluxes
    ϕhs = gen_ϕs(aq, ψhs)
    # interior cell-edge current comp (w.r.t face normal direction / axis)
    jns = gen_jns(aq, cm, ψds) # NOTE : interior faces only!
    # interior cell-edge standard diffusion coefficients
    dcs = gen_ds(cm, get_Σ_ts(ntm)) # NOTE : interior faces only!
    dps = dms = dcs

    # interior cell-edge transport correct diffusion coefficients
    dhs = gen_dhs(cm, ϕhs, jns, dcs)

    mm = MSEDMaterial(dps, dms, dhs,
                      get_Σ_ts(ntm), get_Σ_ss(ntm), get_Σ_fs(ntm),
                      get_νs(ntm), get_χs(ntm), get_Qs(ntm),
                      G, H, H_f)

    return mm
end

# generate trasport corrected Diffusion Boundary Condition Collection
function gen_DiBoCo(aq::AnQu, tm::TrMa, cm::CaMe, fd::FiDi, ψds::Arr{Fl,3}, tbc::TrBoCo)
    h_fs_dbs = []
    dbs      = []
    for (h_fs_tb, tb) in zip(get_h_fs_tbs(tbc), get_tbs(tbc))
        if     isa(tb, ReTrBo)
            db = ReflDiffBound()
        elseif isa(tb, VaTrBo) || isa(tb, IsTrBo)
            # NOTE : for now bs and jbis are calculated on ALL boundary faces
            # (might consider only generating on h_fs_tb faces)
            bs   = gen_bs(aq, tm, cm, fd, ψds)
            jbis = gen_jbis(aq, cm, ψds)
            db = InTCDiffBound(bs, jbis)
        else
            # TODO : implement additional transport / diffusion boundary
            # conversions
            error()
        end

        push!(h_fs_dbs, h_fs_tb)
        push!(dbs, db)
    end

    dbc = DiBoCo(h_fs_dbs, dbs)
    return dbc
end

# TODO : anisotropic scattering
# transport scattering source
function gen_s_s(aq::AnQu, ntm::NeTrMa, cm::CaMe, ϕhs::Arr{Fl,2})
    G = get_G(ntm)
    H = get_H(cm)

    s_s = zeros(G, H)

    for h in get_hs(cm)
        for g in get_gs(ntm)
            for gp in get_gs(ntm)
                Σ_s_gp_g_h = get_Σ_ss(ntm, gp, g, h)
                ϕh_gp_h    = ϕhs[gp, h]

                s_s[g, h] += Σ_s_gp_g_h * ϕh_gp_h
            end
        end
    end

    return s_s
end
# transport fission source
function gen_s_f(aq::AnQu, ntm::NeTrMa, cm::CaMe, ϕhs::Arr{Fl,2})
    G = get_G(ntm)
    H = get_H(cm)

    s_f = zeros(G, H)

    for h in get_hs(cm)
        for g in get_gs(ntm)
            χ_g_h = get_χs(ntm, g, h)
            for gp in get_gs(ntm)
                ν_gp_h   = get_νs(ntm, gp, h)
                Σ_f_gp_h = get_Σ_fs(ntm, gp, h)
                ϕh_gp_h  = ϕhs[gp, h]

                s_f[g, h] += χ_g_h * ν_gp_h * Σ_f_gp_h * ϕh_gp_h
            end
        end
    end

    return s_f
end
# TODO : anisotropic inhomogeneous source
# transport inhomogeneous source
function gen_s_q(aq::AnQu, ntm::NeTrMa, cm::CaMe)
    G = get_G(ntm)
    H = get_H(cm)

    s_q = zeros(G, H)

    for h in get_hs(cm)
        for g in get_gs(ntm)
            Q_g_h = get_Qs(ntm, g, h)

            s_q[g, h] += Q_g_h
        end
    end

    return s_q
end
function gen_ss(aq::AnQu, ntm::NeTrMa, cm::CaMe, ϕhs::Arr{Fl,2}; λ::Fl = 1.)
    s_s = gen_s_s(aq, ntm, cm, ϕhs)
    s_f = gen_s_f(aq, ntm, cm, ϕhs)
    s_q = gen_s_q(aq, ntm, cm)

    ss = s_s + λ*s_f + s_q

    return ss
end

# given transport cell-edge angular fluxes, calculate an eigenvalue estimate
function est_tran_λ(aq::AnQu, ntm::NeTrMa, cm::CaMe, fd::FiDi, ψds::Arr{Fl,3})
    G = get_G(ntm)
    H = get_H(cm)

    # TODO : ∇ ⋅ J contrib. ugly (probably easier for general meshes?)
    # ∇ ⋅ J integ.
    c_g_j = 0.
    for h_f_b in get_h_f_bs(cm)
        if     is_h_f_rl(cm, h_f_b)
            f_j = gen_jis

            if     h_f_b in get_h_f_rbs(cm)
                sj = + 1.
            elseif h_f_b in get_h_f_lbs(cm)
                sj = - 1.
            end
        elseif is_h_f_ud(cm, h_f_b)
            f_j = gen_jjs

            if     h_f_b in get_h_f_ubs(cm)
                sj = + 1.
            elseif h_f_b in get_h_f_dbs(cm)
                sj = - 1.
            end

        elseif is_h_f_oi(cm, h_f_b)
            f_j = gen_jks

            if     h_f_b in get_h_f_obs(cm)
                sj = + 1.
            elseif h_f_b in get_h_f_ibs(cm)
                sj = - 1.
            end
        end

        c_g_j += sj * sum(f_j(aq, ψds[:,:,h_f_b])) * get_dpa(cm, h_f_b)
    end

    ψhs = gen_ψhs(aq, ntm, cm, fd, SC, 1:G, 1:H, ψds)
    ϕhs = gen_ϕs(aq, ψhs)

    dpvs = get_dpvs(cm)

    # s_s integ.
    s_s = gen_s_s(aq, ntm, cm, ϕhs)
    c_s_s = sum(s_s .* reshape(dpvs, 1, H))

    # s_f integ.
    s_f = gen_s_f(aq, ntm, cm, ϕhs)
    c_s_f = sum(s_f .* reshape(dpvs, 1, H))

    # s_t integ.
    c_s_t = sum(get_Σ_ts(ntm) .* ϕhs .* reshape(dpvs, 1, H))

    λ = (c_g_j + c_s_t - c_s_s) / (c_s_f)

    return λ
end

gen_it_ρs(es) = begin
    it_ρs = [es[i+1] / es[i] for i in 1:(length(es) - 1)]
    return it_ρs
end

MIN_US = 2

# df / db : percentage of iterated collection to drop at the
# start and end for global spectral radius estimation
gen_es_ρ(es; df::Fl=0.25, db::Fl=0.25) = begin
    n_es = length(es) # no. es
    n_df = Int(ceil(n_es * df)) # no. drop es (front)
    n_db = Int(ceil(n_es * db)) # no. drop es (back)

    # no. es used
    n_us = (n_es - n_df - n_db)

    if n_us < MIN_US
        error("insufficient number of iterations")
    end

    us =  es[(n_df+1):(end-n_db)]

    # slope, and intercept
    sl, inter = [1:n_us ones(n_us)] \ log.(us)

    return exp(sl)
end

end

